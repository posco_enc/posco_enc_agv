/* Author: SIS S/W Team  */

#include "boost/thread/thread.hpp"
#include "ros/ros.h"
#include "ros/time.h"

// include message to advertise wheel speed data

#include "move_control/convCmd.h"
#include "move_control/convJoy.h"
#include "move_control/convStatus.h"
#include "usb_dio/io_in.h"
#include "usb_dio/io_out.h"
#include "usb_dio/limConv.h"
#include "usb_dio/limStp.h"

#include <motor_driver_msgs/convCtrl.h>
#include <motor_driver_msgs/convdata.h>

#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/Bool.h>

using namespace std;
using namespace ros;

#define HOME 0
#define LIMIT 1
#define MID 2

#define STOP 0
#define MOVING 1
#define GOAL_REACH 3
#define ERROR 2

#define MODE_STOP 0
#define MODE_START 1
#define MODE_EMC 2
#define MODE_MANUAL 3

string node_name("conv");

class conv
{

  ros::NodeHandle nh_;
  ros::Publisher pubConvCmd_, pubStatus_, pubAlm, pubConvBrake_ ;
                //, pubStp1Cmd_, pubStp2Cmd_, pubStp1Brake_, pubStp2Brake_;

  ros::Subscriber subAuto_, subMan_, sublimConv_, subIO_, subPos_, subSpd, subOutput, sub_emergency_stop, sub_emergency_mode_acs, sub_emergency_mode_joy;
                //, sublimStp1_, sublimStp2_, subStp1Joy, subStp2Joy;
  // ros::ServiceClient cltBrake_;

private:
  int8_t limArrival_, limIn_, limMid_; //, limDown1_, limUp1_, limDown2_, limUp2_;
  int8_t cntIn_ = 0, cntArrival_ = 0;
  bool manual_mode = false;
  bool emcStop_ = false;
  double target_pos_ = 0;
  double conv_pos_ = 0, conv_cal_ = 0;
  double screw_ratio_ = 4, gear_ratio_ = 5;
  double joy_vel_ = 0, auto_vel_ = 0;
  motor_driver_msgs::convCtrl conv_vel_;
  move_control::convStatus conv_state_;
  int cpr_ = 4096; // 1024 * 4
  int brake_ = 1;
  int8_t conv_brake_off_ = 1; //, stp1_brake_off_ = 1, stp2_brake_off_ = 1;
  double bias_ = 0;
  int8_t cnt_p = 0;
  bool encoder_use_ = false;
  ros::Time current_time, last_time;
  bool first_run_ = true;
  int brake_off_count = 0;
  int brake_off_delay = 50 ; // 50 * delay/1000 = 2.5 sec
  double driver_vel_count = 0.0;  // 천천히 가속
  bool emcModeAcs_ = false;
  bool emcModeJoy_ = false;

public:
  /**
   * @brief Constructor
   */
  conv()
  {
    ros::NodeHandle nhLocal("~");

    nhLocal.param("gear_ratio", gear_ratio_, gear_ratio_);
    nhLocal.param("screw_ratio", screw_ratio_, screw_ratio_);
    nhLocal.param("cpr", cpr_, cpr_);
    nhLocal.param("brake", brake_, brake_);

    // subcribe navigation command
    subAuto_ = nh_.subscribe("/CONV/CMD", 1, &conv::convAutoCallback, this);
    // subcribe joy command
    subMan_ = nh_.subscribe("CONV/MAN", 1, &conv::convManCallback, this);
    //subStp1Joy = nh_.subscribe("/STP1/MAN", 1, &conv::stp1JoyCallback, this);
    //subStp2Joy = nh_.subscribe("/STP2/MAN", 1, &conv::stp2JoyCallback, this);

    sublimConv_ = nh_.subscribe("CONV/LIM", 1, &conv::limConvCallback, this);
    //sublimStp1_ = nh_.subscribe("STP1/LIM", 1, &conv::limStp1Callback, this);
    //sublimStp2_ = nh_.subscribe("STP2/LIM", 1, &conv::limStp2Callback, this);
    subPos_ = nh_.subscribe("/POS", 1, &conv::posCallback, this);
    subOutput = nh_.subscribe("/io_out", 1, &conv::outputCallback, this);

    pubStatus_ = nh_.advertise<move_control::convStatus>("/CONV/STATUS", 1, true);
    pubAlm = nh_.advertise<std_msgs::UInt8>("/ALM", 1, true);
    pubConvCmd_ =
        nh_.advertise<motor_driver_msgs::convCtrl>("/CONV/CTRL", 1, true);
    pubConvBrake_ = nh_.advertise<std_msgs::UInt8>("/CONV/BRAKE", 1, true);
    //pubStp1Cmd_ = nh_.advertise<std_msgs::Int8>("/STP1/CMD", 1, true);
    //pubStp1Brake_ = nh_.advertise<std_msgs::UInt8>("/STP1/BRAKE", 1, true);
    //pubStp2Cmd_ = nh_.advertise<std_msgs::Int8>("/STP2/CMD", 1, true);
    //pubStp2Brake_ = nh_.advertise<std_msgs::UInt8>("/STP2/BRAKE", 1, true);

    sub_emergency_stop = nh_.subscribe("/EMERGENCY/STOP", 10, &conv::emergencyStopCallback, this);
    sub_emergency_mode_acs = nh_.subscribe("/EMERGENCY/STOP", 10, &conv::emergencyModeAcsCallback, this);
    sub_emergency_mode_joy = nh_.subscribe("/EMERGENCY/STOP", 10, &conv::emergencyModeJoyCallback, this);

    limArrival_ = 0, limIn_ = 0, limMid_ = 0;
    //limUp1_ = 0, limDown1_ = 0, limUp2_ = 0, limDown2_ = 0;
  }

  ~conv() {}

  void posCallback(motor_driver_msgs::convdata msg)
  {
    double enc_cnt = (double)msg.data;
    conv_cal_ = enc_cnt * screw_ratio_ / (cpr_ * gear_ratio_);
    if (limIn_ == 1)
    {
      conv_pos_ = 0;
      bias_ = conv_cal_;
    }
    else
    {
      conv_pos_ = conv_cal_ - bias_;
    }
  }

  void emergencyStopCallback(const std_msgs::Bool::ConstPtr& emergencyStopData) {
    emcStop_ = emergencyStopData->data;
  }

  void emergencyModeAcsCallback(const std_msgs::Bool::ConstPtr& emergencyModeData) {
    emcModeAcs_ = emergencyModeData->data;
  }

  void emergencyModeJoyCallback(const std_msgs::Bool::ConstPtr& emergencyModeData) {
    emcModeJoy_ = emergencyModeData->data;
  }
  /*------ not use -----
  void stp1JoyCallback(const std_msgs::Int8 msg)
  {
      if(manual_mode == true && bump_ == 0 && emc_ == 0)
      {
          if(msg.data == 1)
          {
              std_msgs::UInt8 stp1_brake_msgs;
              std_msgs::Int8 stp1_cmd_msgs;
              if (stp1_brake_off_ == 0)
              {
                stp1_brake_msgs.data = 1 - stp1_brake_off_;
                pubStp1Brake_.publish(stp1_brake_msgs);
              }
              if (stp1_brake_off_ == 1)
              {
                stp1_cmd_msgs.data = msg.data;
                pubStp1Cmd_.publish(stp1_cmd_msgs);
              }
          }
          else if(msg.data == -1)
          {
              std_msgs::UInt8 stp1_brake_msgs;
              std_msgs::Int8 stp1_cmd_msgs;
              if (stp1_brake_off_ == 0)
              {
                stp1_brake_msgs.data = 1 - stp1_brake_off_;
                pubStp1Brake_.publish(stp1_brake_msgs);
              }
              if (stp1_brake_off_ == 1)
              {
                stp1_cmd_msgs.data = msg.data;
                pubStp1Cmd_.publish(stp1_cmd_msgs);
              }
          }
          else
          {
              std_msgs::UInt8 stp1_brake_msgs;
              std_msgs::Int8 stp1_cmd_msgs;
              if (stp1_brake_off_ == 0)
              {
                stp1_brake_msgs.data = 1 - stp1_brake_off_;
                pubStp1Brake_.publish(stp1_brake_msgs);
              }
              if (stp1_brake_off_ == 1)
              {
                stp1_cmd_msgs.data = 0;
                pubStp1Cmd_.publish(stp1_cmd_msgs);
              }
          }
      }
  }

  void stp2JoyCallback(const std_msgs::Int8 msg)
  {
      if(manual_mode == true && bump_ == 0 && emc_ == 0)
      {
          if(msg.data == 1)
          {
              std_msgs::UInt8 stp2_brake_msgs;
              std_msgs::Int8 stp2_cmd_msgs;
              if (stp2_brake_off_ == 0)
              {
                stp2_brake_msgs.data = 1 - stp2_brake_off_;pub
                pubStp2Brake_.publish(stp2_brake_msgs);
              }
              if (stp2_brake_off_ == 1)
              {
                stp2_cmd_msgs.data = msg.data;
                pubStp2Cmd_.publish(stp2_cmd_msgs);
              }
          }
          else if(msg.data == -1)
          {
              std_msgs::UInt8 stp2_brake_msgs;
              std_msgs::Int8 stp2_cmd_msgs;
              if (stp2_brake_off_ == 0)
              {
                stp2_brake_msgs.data = 1 - stp2_brake_off_;
                pubStp2Brake_.publish(stp2_brake_msgs);
              }
              if (stp2_brake_off_ == 1)
              {
                stp2_cmd_msgs.data = msg.data;
                pubStp2Cmd_.publish(stp2_cmd_msgs);
              }
          }
          else
          {
              std_msgs::UInt8 stp2_brake_msgs;
              std_msgs::Int8 stp2_cmd_msgs;
              if (stp2_brake_off_ == 0)
              {
                stp2_brake_msgs.data = 1 - stp2_brake_off_;
                pubStp2Brake_.publish(stp2_brake_msgs);
              }
              if (stp2_brake_off_ == 1)
              {
                stp2_cmd_msgs.data = 0;
                pubStp2Cmd_.publish(stp2_cmd_msgs);
              }
          }
      }
  }
  ----------here ------*/

  void convAutoCallback(const move_control::convJoy msg)
  {
    if (msg.mode == 1 && manual_mode == false)
    // 모드가 1이고 메뉴얼 모드가 아닐 경우
    {
      auto_vel_ = msg.speed;
      joy_vel_ = 0;
    }
    else
    {
      auto_vel_ = 0;
    }
  }

  void limConvCallback(const usb_dio::limConv msg)
  {
    limMid_ = msg.mid;
    limArrival_ = msg.arrival;
    /*
    if (msg.arrival == 1)
    {
      if (cntArrival_ >= 8) // 250ms -> 400ms
      {
        limArrival_ = 1;
        cntArrival_ = 0;
      }
      if (limArrival_ != 1)
        cntArrival_++;
    }
    else
      limArrival_ = 0;
    */
    if (msg.in == 1)
    {
      if (cntIn_ >= 8) // 250ms -> 400ms
      {
        limIn_ = 1;
        cntIn_ = 0;
      }
      if (limIn_ != 1)
        cntIn_++;
    }
    else
      limIn_ = 0;
  }

  void outputCallback(const usb_dio::io_out msg)
  {
    conv_brake_off_ = msg.conv_brake;
  }

  void stopconv()
  {
    conv_vel_.mode = motor_driver_msgs::convCtrl::MODE_STOPPED;
    conv_vel_.speed = 0;
    driver_vel_count = 0;
  }

  void setSpeed(float x)
  {// joyval: max 1
    conv_vel_.mode = motor_driver_msgs::convCtrl::MODE_VELOCITY;
    
    //conv_vel_.speed = x * 1000.0 * 60.0 * gear_ratio_ / screw_ratio_;  //need check.
    conv_vel_.speed = x * 4096 ;//* 60.0 * gear_ratio_ / screw_ratio_;  //need check.
    
    if (driver_vel_count < 50) {
    // 카운트가 50 이하라면 천천히 가속
      double driver_vel_ratio = (driver_vel_count / 50);
      conv_vel_.speed *= driver_vel_ratio;
      driver_vel_count = driver_vel_count + 1.0;
    }

    //ROS_INFO(" cmd_vel: %f, conv_vel: %f",x, conv_vel_.speed);
  }

  // 메뉴얼 콜백함수
  void convManCallback(const move_control::convJoy msg)
  {
    if (msg.mode == 1)
    {
      manual_mode = true;
      joy_vel_ = msg.speed;
      auto_vel_ = 0;
    }
    else
    {
      manual_mode = false;
      joy_vel_ = 0;
    }
  }

  int run()
  {
    ros::Rate loop_rate(10);
    std_msgs::UInt8 brake_msgs;

    //std_msgs::UInt8 stp1_brake_msgs;
    //std_msgs::UInt8 stp2_brake_msgs;
    //std_msgs::Int8 stp1_cmd_msgs;
    //std_msgs::Int8 stp2_cmd_msgs;


    while (ros::ok())
    {
      if (emcStop_ && (emcModeJoy_ == false || emcModeAcs_ == false))
      {
        stopconv();
        if (conv_brake_off_ == 1)
        {
          cnt_p = 0;
          brake_msgs.data = 1 - conv_brake_off_;

          pubConvBrake_.publish(brake_msgs);
        }
        conv_state_.mode = MODE_EMC;
        conv_state_.status = STOP;
        std_msgs::UInt8 convAlm_;
        convAlm_.data = 1;
        pubAlm.publish(convAlm_);
      } // ems state.
      else
      {
        if (joy_vel_ == 0 && auto_vel_ == 0)
        { // command stop case
          // 속도가 0일 경우
          stopconv();
          if (conv_brake_off_ == 1)
          {
            cnt_p = 0;

            // brake off delay
            if (brake_off_count > brake_off_delay ) // n sec
            {
              brake_msgs.data = 1 - conv_brake_off_;
              pubConvBrake_.publish(brake_msgs); 
              brake_off_count = 0;
            }
            brake_off_count ++;
          }
          conv_state_.status = STOP;
        }
        else
        {
          // 속도가 있을 경우
          if (joy_vel_ < 0 || auto_vel_ < 0)
          {
            // 속도가 + 일 경우
            if (limArrival_ == 1)
            { // limit on case
              // 제품 도착 (리미트 ON)
              stopconv();
              if (conv_brake_off_ == 1)
              {
                cnt_p = 0;
                
                // conv brake off delay ----
                if (brake_off_count > brake_off_delay ) // n sec
                {
                  brake_msgs.data = 1 - conv_brake_off_;
                  pubConvBrake_.publish(brake_msgs); 
                  brake_off_count = 0;
                }
                brake_off_count ++;

              }
              conv_state_.status = STOP;
            }
            else
            { // run
              if (conv_brake_off_ == 0)
              {
                cnt_p = 0;
                brake_msgs.data = 1 - conv_brake_off_;
                pubConvBrake_.publish(brake_msgs);
              }
              if (cnt_p >= 10 && conv_brake_off_ == 1)
              {
                setSpeed(joy_vel_ + auto_vel_);
                conv_state_.status = MOVING;
              }
              else
              {
                cnt_p++;
                stopconv();
              }
            }
          }
          else
          { // run...
            if (conv_brake_off_ == 0)
            {
              cnt_p = 0;
              brake_msgs.data = 1 - conv_brake_off_;
              pubConvBrake_.publish(brake_msgs);
            }
            if (cnt_p >= 10 && conv_brake_off_ == 1)
            {
              setSpeed(joy_vel_ + auto_vel_);
              conv_state_.status = MOVING;
            }
            else
            {
              cnt_p++;
              stopconv();
            }
          }
        }
      } 

      if (manual_mode)
      {
        // 메뉴얼 모드
        conv_state_.mode = MODE_MANUAL;
        std_msgs::UInt8 convAlm_;
        convAlm_.data = 0;
        pubAlm.publish(convAlm_);
      } else {
        if (auto_vel_ != 0) {
          // 자동 동작 중이라면
          conv_state_.mode = MODE_START;
        } else {
          // 그렇지 않다면
          conv_state_.mode = MODE_STOP;
        }
      }
      conv_state_.x = conv_pos_;
      pubConvCmd_.publish(conv_vel_);
      pubStatus_.publish(conv_state_);

      ros::spinOnce();
      loop_rate.sleep();
    }
  }
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, node_name);
  conv conv_management;
  return conv_management.run();
}
