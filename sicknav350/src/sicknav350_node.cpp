/*
 * sicknav350_node.cpp
 *
 *  Created on: July, 2021
 *      Author: To Xuan Dinh & Tran Thi Trang
 *
 * Released under BSD license.
 */

#include "ros/ros.h"
#include "sensor_msgs/LaserScan.h"
#include <deque>
#include <iostream>
#include <sicktoolbox/SickNAV350.hh>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <std_msgs/Bool.h>

#define DEG2RAD M_PI / 180.0

using namespace std;
using namespace SickToolbox;

const double TRANSFORM_TIMEOUT = 20.0f;
const double POLLING_DURATION = 0.05f;

void publish_scan(ros::Publisher* pub, double* range_values,
                  uint32_t n_range_values, unsigned int* intensity_values,
                  uint32_t n_intensity_values, ros::Time start,
                  double scan_time, bool inverted, float angle_min,
                  float angle_max, std::string frame_id,
                  unsigned int sector_start_timestamp)
{
  static int scan_count = 0;
  sensor_msgs::LaserScan scan_msg;
  scan_msg.header.frame_id = frame_id;
  scan_count++;
  if (inverted)
  {
    scan_msg.angle_min = angle_max * DEG2RAD;
    scan_msg.angle_max = angle_min * DEG2RAD;
  }
  else
  {
    scan_msg.angle_min = angle_min * DEG2RAD;
    scan_msg.angle_max = angle_max * DEG2RAD;
  }
  scan_msg.angle_increment =
      (scan_msg.angle_max - scan_msg.angle_min) / (double)(n_range_values - 1);
  scan_msg.scan_time = 0.125; // scan_time 125ms;
  scan_msg.time_increment = scan_msg.scan_time / n_range_values;
  scan_msg.range_min = 0.1;
  scan_msg.range_max = 250.;
  scan_msg.ranges.resize(n_range_values);
  scan_msg.header.stamp = start;
  for (size_t i = 0; i < n_range_values; i++)
  {
    scan_msg.ranges[i] = (float)range_values[i] / 1000;
  }
  scan_msg.intensities.resize(n_intensity_values);
  for (size_t i = 0; i < n_intensity_values; i++)
  {
    scan_msg.intensities[i] = 0; //(float)intensity_values[i];
  }
  pub->publish(scan_msg);
}

// you can also define a customized urdf model using the nav350 meshes given

// position as tf
void PublishPositionTransform(double x, double y, double th,
                              tf::TransformBroadcaster odom_broadcaster,
                              std::string header_frame_id,
                              std::string child_frame_id)
{

  ros::Time current_time;
  current_time = ros::Time::now();
  geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th);
  geometry_msgs::TransformStamped odom_trans;
  odom_trans.header.stamp = current_time;
  odom_trans.header.frame_id = header_frame_id; // "map"
  odom_trans.child_frame_id =
      child_frame_id; // "reflector or base or odom frame"

  odom_trans.transform.translation.x = x; // global x coordinate
  odom_trans.transform.translation.y = y; // global y coordinate
  odom_trans.transform.translation.z = 0;
  odom_trans.transform.rotation = odom_quat;

  // send the transform
  odom_broadcaster.sendTransform(odom_trans);
}

void PublishReflectorTransform(
    std::vector<double> x, std::vector<double> y, double th,
    std::vector<tf::TransformBroadcaster> odom_broadcaster,
    std::string frame_id, std::string child_frame_id)
{
  // printf("\npos %.2f %.2f %.2f\n",x,y,th);
  tf::Transform b_transforms;
  tf::Quaternion tfquat = tf::createQuaternionFromYaw(
      0); // th should be 0? no orientation information on reflectors?

  std::ostringstream childframes;
  for (int i = 0; i < x.size(); i++)
  {
    childframes << child_frame_id << i;
    b_transforms.setRotation(tfquat);
    b_transforms.setOrigin(tf::Vector3(-x[i]/1000.0, -y[i]/1000.0, 0.0));

    // send the transform
    odom_broadcaster[i].sendTransform(tf::StampedTransform(
        b_transforms, ros::Time::now(), frame_id, childframes.str()));
    childframes.str(std::string());
    childframes.clear();
  }
}

// 연결 여부 전송
void PublishConnect(ros::Publisher* pub, bool flag) {
  std_msgs::Bool connect_msgs;
  connect_msgs.data = flag;
  pub->publish(connect_msgs);
}

int main(int argc, char* argv[])
{
  ros::init(argc, argv, "sicknav350");
  int port;
  std::string ipaddress;
  std::string scan_frame_id;
  std::string odometry;
  std::string scan;
  bool inverted;
  bool publish_tf_, publish_odom_, publish_scan_;
  int sick_motor_speed = 8;     // 10; // Hz
  double sick_step_angle = 1.5; // 0.5;//0.25;
  double active_sector_start_angle = 0;
  double active_sector_stop_angle = 360; // 269.75;
  std::string global_frame_id, base_frame_id, odom_frame_id;
  std::string reflector_frame_id;

  tf::StampedTransform sickn350_to_base_tf;
  tf::TransformListener tf_listerner;

  // Odom 이전 위치
  double pre_x1 = 0.0;
  double pre_y1 = 0.0;
  double pre_phi1 = 0.0;
  int odom_count = 0;

  ros::NodeHandle nh;
  ros::NodeHandle nh_ns("~");
  nh_ns.param<std::string>("scan", scan, "scan");
  //ros::Publisher scan_pub = nh.advertise<sensor_msgs::LaserScan>(scan, 100);

  nh_ns.param<bool>("publish_tf", publish_tf_, true);
  nh_ns.param<bool>("publish_odom_", publish_odom_, true);
  nh_ns.param<bool>("publish_scan", publish_scan_, true);

  nh_ns.param("port", port, DEFAULT_SICK_TCP_PORT);
  nh_ns.param("ipaddress", ipaddress, (std::string)DEFAULT_SICK_IP_ADDRESS);
  nh_ns.param("inverted", inverted, false);
  nh_ns.param<std::string>("scan_frame_id", scan_frame_id,
                           "nav350"); // laser frame for scan data

  nh_ns.param<std::string>(
      "global_frame_id", global_frame_id,
      "map"); // global cooridnate frame measurement for
              // navigation and position based on reflectors
  nh_ns.param<std::string>(
      "base_frame_id", base_frame_id,
      "base_link"); // a fixed frame eg: odom or base or reflector

  nh_ns.param<std::string>("reflector_frame_id", reflector_frame_id,
                           "reflector");

  nh_ns.param("resolution", sick_step_angle, 1.0);
  nh_ns.param("start_angle", active_sector_start_angle, 0.);
  nh_ns.param("stop_angle", active_sector_stop_angle, 360.);
  nh_ns.param("scan_rate", sick_motor_speed, 5);

  ros::Publisher pubConnect = nh.advertise<std_msgs::Bool>("/NAV350/CONNECT", 1);

  /* Define buffers for return values */
  double range_values[SickNav350::SICK_MAX_NUM_MEASUREMENTS] = {0};
  unsigned int intensity_values[SickNav350::SICK_MAX_NUM_MEASUREMENTS] = {0};
  /* Define buffers to hold sector specific data */
  unsigned int num_measurements = {0};
  unsigned int sector_start_timestamp = {0};
  unsigned int sector_stop_timestamp = {0};
  double sector_step_angle = {0};
  double sector_start_angle = {0};
  double sector_stop_angle = {0};
  /* Instantiate the object */
  SickNav350 sick_nav350(ipaddress.c_str(), port);
  ros::Duration(1).sleep(); // timedelay for AGV startup jobs
  double last_time_stamp = 0;
  try
  {
    /* Initialize the device */
    sick_nav350.Initialize();
    sick_nav350.GetSickIdentity();
    /*
     Operation Mode:
     0: POWER DOWN
     1: STANDBY
     2: MAPPING
     3: LANDMARK DETECTION
     4: NAVIGATION
     */
    try
    {
      sick_nav350.SetOperatingMode(1);
      sick_nav350.SetScanDataFormat();
      sick_nav350.SetOperatingMode(4);
      ROS_INFO("Set Navigation Mode");
    }
    catch (...)
    {
      ROS_ERROR("Configuration error");
      PublishConnect(&pubConnect, false);
      return -1;
    }

    ros::Time last_start_scan_time;
    unsigned int last_sector_stop_timestamp = 0;
    ros::Rate loop_rate(10);
    tf::TransformBroadcaster odom_broadcaster;
    tf::TransformBroadcaster base_broadcaster;
    tf::TransformBroadcaster laser_broadcaster;
    std::vector<tf::TransformBroadcaster> landmark_broadcasters;

    try
    {
      std::string error_msg;
      ros::Time current_time = ros::Time(0);
      if (!tf_listerner.waitForTransform(
              base_frame_id, scan_frame_id, ros::Time(0),
              ros::Duration(TRANSFORM_TIMEOUT), ros::Duration(POLLING_DURATION),
              &error_msg))
      {
        ROS_ERROR_STREAM(
            "Transform lookup timed out, error msg: " << error_msg);
        PublishConnect(&pubConnect, false);
        return -1;
      }

      tf_listerner.lookupTransform(scan_frame_id, base_frame_id, current_time,
                                   sickn350_to_base_tf);
    }
    catch (tf::LookupException& exp)
    {
      ROS_ERROR_STREAM("Transform lookup between " << scan_frame_id << " and "
                                                   << base_frame_id
                                                   << " failed, exiting");
      PublishConnect(&pubConnect, false);
      return -1;
    }

    while (ros::ok())
    {
      /* Get the scan and landmark measurements */
      PublishConnect(&pubConnect, true);
      sick_nav350.GetDataNavigation(1, 2);
      sick_nav350.GetSickMeasurements(
          range_values, &num_measurements, &sector_step_angle,
          &sector_start_angle, &sector_stop_angle, &sector_start_timestamp,
          &sector_stop_timestamp);

      double x1 = (double)sick_nav350.PoseData_.x;
      double y1 = (double)sick_nav350.PoseData_.y;
      double phi1 = (double)sick_nav350.PoseData_.phi;

      tf::Transform odom_to_sick_tf;
      tf::Transform odom_to_base_tf;
      tf::Quaternion odomquat =
          tf::createQuaternionFromYaw((phi1 / 1000.0) * DEG2RAD);
      odomquat.inverse();
      odom_to_sick_tf.setRotation(odomquat);
      // Modify 210713
      odom_to_sick_tf.setOrigin(tf::Vector3(-x1 / 1000, -y1 / 1000, 0.0));

      // converting to target frame
      odom_to_base_tf = odom_to_sick_tf * sickn350_to_base_tf;

      odom_broadcaster.sendTransform(tf::StampedTransform(
          odom_to_base_tf, ros::Time::now(), global_frame_id, base_frame_id));

      // 이전 값과 비교
      if(pre_x1 == x1 and pre_y1 == y1 and pre_phi1 == phi1) {
        odom_count++;
      } else {
        odom_count = 0;
      }

      if(odom_count >= 100) {
        // 100개 이상 쌓인다면 (이전 값과 계속 동일하다고 판단)
        ROS_ERROR("Error");
        PublishConnect(&pubConnect, false);
        return -1;
      }
      pre_x1 = x1;
      pre_y1 = y1;
      pre_phi1 = phi1;


      /*
       * Get landmark data and broadcast transforms
       */
/*
      int num_reflectors = sick_nav350.PoseData_.numUsedReflectors;
      int number_reflectors = sick_nav350.ReflectorData_.num_reflector;
      std::vector<double> Rx(number_reflectors), Ry(number_reflectors);
      landmark_broadcasters.resize(number_reflectors);

      for (int r = 0; r < number_reflectors; r++)
      {
        Rx[r] = (double)sick_nav350.ReflectorData_.x[r];
        Ry[r] = (double)sick_nav350.ReflectorData_.y[r];
      }

      PublishReflectorTransform(Rx, Ry, DEG2RAD*(phi1 / 1000.0),
                                     landmark_broadcasters, scan_frame_id,
                                     reflector_frame_id);

*/

      if (sector_start_timestamp < last_time_stamp)
      {
        loop_rate.sleep();
        ros::spinOnce();
        continue;
      }
      last_time_stamp = sector_start_timestamp;
      ros::Time end_scan_time = ros::Time::now();

      double scan_duration = 0.125;

      ros::Time start_scan_time = end_scan_time - ros::Duration(scan_duration);
      sector_start_angle -= 180;
      sector_stop_angle -= 180;
      /*
      if (publish_scan_)
      {
        publish_scan(&scan_pub, range_values, num_measurements,
                     intensity_values, num_measurements, start_scan_time,
                     scan_duration, inverted, (float)sector_start_angle,
                     (float)sector_stop_angle, scan_frame_id,
                     sector_start_timestamp);
      }
      */

      last_start_scan_time = start_scan_time;
      last_sector_stop_timestamp = sector_stop_timestamp;



      loop_rate.sleep();
      ros::spinOnce();
    }
    /* Uninitialize the device */
    sick_nav350.Uninitialize();
  }
  catch (...)
  {
    ROS_ERROR("Error");
    PublishConnect(&pubConnect, false);
    return -1;
  }
  return 0;
}
