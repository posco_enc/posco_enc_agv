#include "ros/time.h"
#include <motor_driver_msgs/convdata.h>
#include <motor_driver_msgs/convCtrl.h>
#include <ros/console.h>
#include <ros/ros.h>
#include <serial/serial.h>
#include <signal.h>
#include <sstream>
#include <string>
#include <std_msgs/Bool.h>

class ConvDriver
{

public:
  ConvDriver();

public:
  //
  // cmd_vel subscriber
  //
  void cmdvel_callback(const motor_driver_msgs::convCtrl& command);
  void driver_configure();

  void data_query();
  void data_parsing();
  void timer_callback(const ros::TimerEvent& e);

  void encoder_publish();
  void speed_publish();
  void connect_publish(bool flag);
  
  int run();

protected:
  ros::NodeHandle nh;

  serial::Serial controller;

  static int32_t to_rpm(int32_t x) { return int32_t(x); }

  //
  // cmd subscriber
  //
  ros::Subscriber cmdvel_sub;

  //
  // speed publisher
  //
  ros::Publisher encoder_data;
  ros::Publisher speed_data;
  ros::Publisher connect_pub;
  ros::Timer sampling_time;

  // buffer for reading encoder counts
  int data_idx;
  char data_buf[24];

  // toss out initial encoder readings
  char data_encoder_toss;

  int32_t first_encoder;
  int32_t second_encoder;
  int32_t first_rpm;
  int32_t second_rpm;

  // settings

  std::string cmdvel_topic;
  std::string port;
  int baud;
  bool open_loop;
  int encoder_ppr;
  int encoder_cpr;
  double max_rpm = 2000;

  float second_motor_speed = 0;
  float first_motor_speed = 0;
  int32_t motor_mode = -1;
  int dir = 0;

};



