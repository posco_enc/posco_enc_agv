#include <conv_driver.h>

void mySigintHandler(int sig)
{
  ROS_INFO("Received SIGINT signal, shutting down...");
  ros::shutdown();
}


//    : data_idx(0), data_encoder_toss(5), first_encoder(0),
//      second_encoder(0), open_loop(false), baud(115200), encoder_ppr(0),
//      encoder_cpr(0)
// max_rpm(3000)

ConvDriver::ConvDriver()
    : data_idx(0), data_encoder_toss(5), first_encoder(0),second_encoder(0)
{

  // CBA Read local params (from launch file)
  ros::NodeHandle nhLocal("~");

  nhLocal.param<std::string>("port", port, "/dev/ttyUSB5");
  nhLocal.param("baud", baud, 115200);
  nhLocal.param("open_loop", open_loop, false);
  nhLocal.param("encoder_ppr", encoder_ppr, 3000); //1024
  nhLocal.param("encoder_cpr", encoder_cpr, 12000); // 4096
  nhLocal.param("max_rpm", max_rpm, 2000.0);
  nhLocal.param("dir", dir, 0);

  cmdvel_sub =
      nh.subscribe("/CONV/CTRL", 10, &ConvDriver::cmdvel_callback, this);
  encoder_data =
      nh.advertise<motor_driver_msgs::convdata>("/CONV/POS", 10);
  speed_data =
      nh.advertise<motor_driver_msgs::convdata>("CONV/speed_data", 10);
  sampling_time =
      nh.createTimer(ros::Duration(0.01), &ConvDriver::timer_callback, this);
  connect_pub = nh.advertise<std_msgs::Bool>("/CONV/CONNECT", 10);
  //int data_idx=0, data_encoder_toss=5;
  //int first_encoder=0, second_encoder=0;
}

//
// cmd_vel subscriber
//

void ConvDriver::cmdvel_callback(const motor_driver_msgs::convCtrl& command)
{

  if (command.mode == motor_driver_msgs::convCtrl::MODE_VELOCITY)
  {
    // wheel speed (m/s)
    first_motor_speed = command.speed / max_rpm * 1000;

    motor_mode = 1;

    // endif open-loop
  }
  else // STOP MODE
  {
    first_motor_speed = 0;
    motor_mode = 0;

  } // endif command.mode
}

void ConvDriver::timer_callback(const ros::TimerEvent& e)
{

  if (motor_mode == 1)
  {
    if (open_loop)
    {
      // motor power (scale 0-1000)

      int32_t first_power = static_cast<int>(first_motor_speed * 1);

      std::stringstream first_cmd;

      first_cmd << "!G 1 " << first_power << "\r";

      controller.write(first_cmd.str());
      controller.flush();
    }
    else
    {
      // motor speed (rpm)

      int32_t first_rpm = static_cast<int>(first_motor_speed * 1);

      std::stringstream first_cmd;

      first_cmd << "!S 1 " << first_rpm << "\r";

      controller.write(first_cmd.str());
      controller.flush();
    }
  }
  else
  {
    int32_t first_rpm = 0;

    std::stringstream first_cmd;

    first_cmd << "!S 1 " << first_rpm << "\r";

    controller.write(first_cmd.str());
    controller.flush();
  }
  // speed_publish();
  // encoder_publish();
}

void ConvDriver::driver_configure()
{

  // stop motors
  controller.write("!G 1 0\r");
  controller.write("!G 2 0\r");
  controller.write("!S 1 0\r");
  controller.write("!S 2 0\r");
  controller.flush();

  // disable echo
  controller.write("^ECHOF 1\r");
  controller.flush();

  // enable watchdog timer (1000 ms)
  controller.write("^RWD 250\r");
  //  controller.write("^RWD 250\r");

  // set motor operating mode (1 for closed-loop speed)
  if (open_loop)
  {
    controller.write("^MMOD 1 0\r");
    controller.write("^MMOD 2 0\r");
  }
  else
  {
    controller.write("^MMOD 1 1\r");
    controller.write("^MMOD 2 1\r");
  }

  // set motor amps limit (50 A * 10)
  controller.write("^ALIM 1 500\r");
  controller.write("^ALIM 2 500\r");

  // set max speed (rpm) for relative speed commands
  //  controller.write("^MXRPM 1 82\r");
  //  controller.write("^MXRPM 2 82\r");
  controller.write("^MXRPM 1 2000\r");
  controller.write("^MXRPM 2 2000\r");

  // set max acceleration rate (500 rpm/s * 10)
  //  controller.write("^MAC 1 2000\r");
  //  controller.write("^MAC 2 2000\r");
  controller.write("^MAC 1 10000\r");
  controller.write("^MAC 2 10000\r");

  // set max deceleration rate (2000 rpm/s * 10)
  controller.write("^MDEC 1 10000\r");
  controller.write("^MDEC 2 10000\r");

  // set number of poles
  controller.write("^BPOL 1 2\r");
  controller.write("^BPOL 2 2\r");

  // set PID parameters (gain * 10)
  controller.write("^KP 1 3\r");
  controller.write("^KP 2 3\r");
  controller.write("^KI 1 50\r");
  controller.write("^KI 2 50\r");
  controller.write("^KD 1 0\r");
  controller.write("^KD 2 0\r");

  // set encoder mode (18 for feedback on motor1, 34 for feedback on motor2)
  controller.write("^EMOD 1 18\r");
  controller.write("^EMOD 2 34\r");

  // set motor direction
  if (dir == 1)
    controller.write("^MDIR 1 1\r");
  else
    controller.write("^MDIR 1 0\r");


  // set encoder counts (ppr)
  std::stringstream second_enccmd;
  std::stringstream first_enccmd;
  second_enccmd << "^EPPR 1 " << encoder_ppr << "\r";
  first_enccmd << "^EPPR 2 " << encoder_ppr << "\r";
  controller.write(second_enccmd.str());
  controller.write(first_enccmd.str());
  controller.flush();
}

void ConvDriver::data_query()
{
  // controller.write("# C_?CR_?BA_?V_?S_# 10\r");
  controller.write("# C_?C_?S_# 10\r");
  controller.flush();
}

void ConvDriver::encoder_publish()
{
  motor_driver_msgs::convdata encoder_value;
  encoder_value.data = first_encoder;
  encoder_data.publish(encoder_value);
}

void ConvDriver::speed_publish()
{
  motor_driver_msgs::convdata speed_value;
  speed_value.data = first_rpm;
  speed_data.publish(speed_value);
}

void ConvDriver::connect_publish(bool flag) {
  std_msgs::Bool connect_msgs;
  connect_msgs.data = flag;
  connect_pub.publish(connect_msgs);
}

void ConvDriver::data_parsing()
{

  // read sensor data stream from motor controller
  if (controller.available())
  {
    connect_publish(true);
    char ch = 0;
    if (controller.read((uint8_t*)&ch, 1) == 0)
      return;
    if (ch == '\r')
    {
      data_buf[data_idx] = 0;

      // Absolute encoder value C

      if (data_buf[0] == 'C' && data_buf[1] == '=')
      {
        int delim;
        for (delim = 2; delim < data_idx; delim++)
        {
          if (data_encoder_toss > 0)
          {
            --data_encoder_toss;
            break;
          }
          if (data_buf[delim] == ':')
          {
            data_buf[delim] = 0;
            first_encoder = (int32_t)strtol(data_buf + 2, NULL, 10);
            second_encoder = (int32_t)strtol(data_buf + delim + 1, NULL, 10);
            // printf("encoder right: %d left: %d \n
            // ",second_encoder,first_encoder);
            encoder_publish();
            break;
          }
        }
      }
      // S= is speed in RPM
      if (data_buf[0] == 'S' && data_buf[1] == '=')
      {
        int delim;
        for (delim = 2; delim < data_idx; delim++)
        {
          if (data_buf[delim] == ':')
          {
            data_buf[delim] = 0;
            first_rpm = (int32_t)strtol(data_buf + 2, NULL, 10);
            second_rpm = (int32_t)strtol(data_buf + delim + 1, NULL, 10);
            // printf("delim: %d encoder right: %d left: %d \n
            // ",delim,second_rpm,first_rpm);
            speed_publish();
            break;
          }
        }
      }

      data_idx = 0;
    }
    else if (data_idx < (sizeof(data_buf) - 1))
    {
      data_buf[data_idx++] = ch;
    }
  }
}

int ConvDriver::run()
{

  serial::Timeout timeout = serial::Timeout::simpleTimeout(1000);
  controller.setPort(port);
  controller.setBaudrate(baud);
  controller.setTimeout(timeout);

  // TODO: support automatic re-opening of port after disconnection
  while (ros::ok())
  {
    // ROS_INFO_STREAM("Opening serial port on " << port << " at " << baud <<
    // "..." );
    try
    {
      controller.open();
      if (controller.isOpen())
      {
        // ROS_INFO("Successfully opened serial port");
        break;
      }
    }
    catch (serial::IOException e)
    {
      ROS_WARN_STREAM("serial::IOException: " << e.what());
    }
    ROS_WARN("Failed to open serial port");
    connect_publish(false);
    sleep(5);
  }

  driver_configure();
  data_query();

  // ros::Rate loopRate(100);

  while (ros::ok())
  {

    data_parsing();
    // cmdVel();

    ros::spinOnce();

    // loopRate.sleep();
  }

  usleep(100000);
  connect_publish(false);
  usleep(100000);

  if (controller.isOpen())
    controller.close();

  ROS_INFO("Exiting");

  return 0;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "ConvDriver_node");

  ConvDriver node;

  signal(SIGINT, mySigintHandler);

  return node.run();
}
