#include "ros/ros.h"
#include "ros/time.h"

#include <errno.h>
#include <fcntl.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <time.h>

#include <iostream>
#include <string>

#include "bdaqctrl.h"
#include "compatibility.h"
#include "geometry_msgs/Twist.h"
#include "usb_dio/io_in.h"
#include "usb_dio/io_out.h"
#include "usb_dio/limConv.h"
#include "usb_dio/limStp.h"
#include <math.h>
#include <move_control/teleop.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/Bool.h>

// using namespace std;
using namespace Automation::BDaq;
//-----------------------------------------------------------------------------------
// Configure the following parameters before running the demo
//-----------------------------------------------------------------------------------
typedef unsigned char byte;
#define deviceDescription L"USB-4750,BID#0"
// const wchar_t* profilePath = L"../../profile/DemoDevice.xml";

class usb4750
{
public:
  usb4750();
  ~usb4750();
  int spin();

protected:
private:
  ros::NodeHandle nh_;
  ros::Publisher pubDI, pubDO, publimConv, publimStp1, publimStp2, pubUsbConnect;
  ros::Subscriber subDI, subNavVel, subJoyVel, sub_emc_state, sub_emc_mode, sub_emc_joy_mode, sub_emc_slow;
  ros::Subscriber subStp1cmd, subStp2cmd;
  ros::Subscriber subConvBrake, subStp1Brake, subStp2Brake, sub_CHG;

  // ros::ServiceServer ser_Ybrake, ser_Zbrake;

  int port(int val);
  int portNum(int val);

  void driverOn(int val);

  void dIPub();
  void dOPub();
  void convLimPub();
  void stp1LimPub();
  void stp2LimPub();
  void connectPub(bool flag);

  void NavControlCallback(const geometry_msgs::Twist::ConstPtr& motorCmd_);
  void JoyControlCallback(const move_control::teleop::ConstPtr& motorCmd_);
  void emcCallback(const std_msgs::UInt8::ConstPtr& msg);
  void emcStateCallback(const std_msgs::Bool::ConstPtr& msg);
  void emcModeCallback(const std_msgs::Bool::ConstPtr& msg);
  void emcJoyModeCallback(const std_msgs::Bool::ConstPtr& msg);
  void emcSlowCallback(const std_msgs::Bool::ConstPtr& msg);

//  void stp1cmdCallback(const std_msgs::Int8::ConstPtr& msg);
//  void stp2cmdCallback(const std_msgs::Int8::ConstPtr& msg);
  void convBrakeCallback(const std_msgs::UInt8::ConstPtr& msg);
//  void stp1BrakeCallback(const std_msgs::UInt8::ConstPtr& msg);
//  void stp2BrakeCallback(const std_msgs::UInt8::ConstPtr& msg);
  void chargeCallback(const std_msgs::UInt8::ConstPtr& msg);

  int rate_ = 10;
  int32_t startPort = 0;
  int32_t portCount = 2;

  int8_t DIs_[16], DOs_[16];
  const wchar_t* profilePath = L"../../profile/DemoDevice.xml";
  /*------------- Input Variables ---------------------------*/
  // Emergency inputs
  int8_t emc_ = 0, pcOff_ = 1, bump_ = 0;
  // Limit sensors inputs
  int8_t conv_arrival_ = 0, conv_mid_ = 0, conv_gatein_ = 0;
        //, stp1_down_ = 0, stp1_up_ = 0, stp2_down_ = 0, stp2_up_ = 0;
  /*------------- Output Variables ---------------------------*/
  // Tower Lamp
  uint32 redV_ = 0, greV_ = 0, yelV_ = 0, buzV_ = 0, bellV_ = 0;
  // Power Supply
  uint32 conv_pwV_ = 0, front_pwV_ = 0, rear_pwV_ = 0;
  // Brake
  uint32 convbrakeV_ = 0; //, stp1brakeV_ = 0, stp2brakeV_ = 0;
  // Charge
  uint32 chgV_ = 0;
  // Stopper control
  //uint32 stp1CwV_ = 0, stp1CcwV_ = 0, stp2CwV_ = 0, stp2CcwV_ = 0;

  /*------------- Input Ports --------------------------------*/
  int emcI_ = 0, bumpI_ = 1, pcOffI_ = 2, convArrivalI_ = 8, convMidI_ = 9,
      convInI_ = 10;
      //, stp1UpI_ = 11, stp1DownI_ = 12, stp2UpI_ = 13, stp2DownI_ = 14;

  /*------------- Output Ports -------------------------------*/
  int chgO_ = 0, front_pwO_ = 1, rear_pwO_ = 2, conv_pwO_ = 3 ,
      //stp1CwO_ = 4, stp1CcwO_ = 5, stp2CwO_ = 6, stp2CcwO_ = 7, 
      //stp1BrakeO_ = 8, stp2BrakeO_ = 9, 
      convBrakeO_ = 10, redO_ = 11, yelO_ = 12, greO_ = 13,
      buzO_ = 14, bellO_ = 15;

  ErrorCode ret, ret2;
  InstantDiCtrl* instantDiCtrl;
  InstantDoCtrl* instantDoCtrl;

  int32_t count_ = 0;

  bool emc_state_data = false;    // 비상 모드 상태
  bool emc_mode_data = false;       // 비상 모드 (ACS)
  bool emc_joy_mode_data = false;   // 비상 모드 (조이스틱)

  int8_t cnt_io = 0, cnt_y = 0, cnt_z = 0, cnt_w = 0;
  bool agvAutoRun_ = false, agvManuRun_ = false;
  int pc_off_count = 0;

  int pre_hour = 0;
  int pre_second = 0;
  bool chg_turn = false;   // 충전 껐다 키기 여부

  bool slow_flag = false;   // 슬로우 여부
};

usb4750::usb4750()
{
  pubDI = nh_.advertise<usb_dio::io_in>("/io_in", 1);
  pubDO = nh_.advertise<usb_dio::io_out>("/io_out", 1);
  publimConv = nh_.advertise<usb_dio::limConv>("/CONV/LIM", 1);
  pubUsbConnect = nh_.advertise<std_msgs::Bool>("/USB4750/CONNECT", 1);
  //publimStp1 = nh_.advertise<usb_dio::limStp>("/STP1/LIM", 1);
  //publimStp2 = nh_.advertise<usb_dio::limStp>("/STP2/LIM", 1);

  // subcribe navigation command
  subNavVel = nh_.subscribe("/cmd_vel", 1, &usb4750::NavControlCallback, this);
  // sub_conv_vel = nh_.subscribe("/CONV/CTRL", 1, &usb4750::ConvControlCallback, this);
  // subcribe commanded velocity
  subJoyVel =
      nh_.subscribe("/cmd_vel_joy", 1, &usb4750::JoyControlCallback, this);

  sub_emc_state = nh_.subscribe("/EMERGENCY/STATE", 1, &usb4750::emcStateCallback, this);
  sub_emc_mode = nh_.subscribe("/EMERGENCY/MODE", 1, &usb4750::emcModeCallback, this);
  sub_emc_joy_mode = nh_.subscribe("/EMERGENCY/JOY_MODE", 1, &usb4750::emcJoyModeCallback, this);
  sub_emc_slow = nh_.subscribe("/EMERGENCY/SLOW", 1, &usb4750::emcSlowCallback, this);

  subConvBrake =
      nh_.subscribe("/CONV/BRAKE", 1, &usb4750::convBrakeCallback, this);
  //subStp1Brake =
  //    nh_.subscribe("/STP1/BRAKE", 1, &usb4750::stp1BrakeCallback, this);
  //subStp2Brake =
  //    nh_.subscribe("/STP2/BRAKE", 1, &usb4750::stp2BrakeCallback, this);
  sub_CHG = nh_.subscribe("/CHG", 1, &usb4750::chargeCallback, this);

  //subStp1cmd = nh_.subscribe("/STP1/CMD", 1, &usb4750::stp1cmdCallback, this);
  //subStp2cmd = nh_.subscribe("/STP2/CMD", 1, &usb4750::stp2cmdCallback, this);

  // 연결 여부 보내기
  usleep(100000);   // 0.1초 쉼
  connectPub(false);   
  usleep(100000);   // 0.1초 쉼

  for (int i = 0; i < 16; i++)
  {
    DOs_[i] = 1;
  }

  for (int i = 0; i < 16; i++)
  {
    DIs_[i] = 1;
  }

  ret = Success, ret2 = Success;
  // Create a 'InstantDiCtrl' for DI function.
  instantDiCtrl = InstantDiCtrl::Create();

  // Create a 'InstantDoCtrl' for DO function.
  instantDoCtrl = InstantDoCtrl::Create();

  DeviceInformation devInfo(deviceDescription);
  ret = instantDiCtrl->setSelectedDevice(devInfo);
  //  CHK_RESULT(ret);
  ret = instantDiCtrl->LoadProfile(
      profilePath); // Loads a profile to initialize the device.
  //   CHK_RESULT(ret);

  ret2 = instantDoCtrl->setSelectedDevice(devInfo);
  // CHK_RESULT(ret);
  ret2 = instantDoCtrl->LoadProfile(
      profilePath); // Loads a profile to initialize the device.
}

void usb4750::connectPub(bool flag)
{
  std_msgs::Bool connect_msgs;
  connect_msgs.data = flag;
  pubUsbConnect.publish(connect_msgs);
}

void usb4750::emcStateCallback(const std_msgs::Bool::ConstPtr& msg)
{
  emc_state_data = msg->data;
}

void usb4750::emcModeCallback(const std_msgs::Bool::ConstPtr& msg)
{
  emc_mode_data = msg->data;
}

void usb4750::emcJoyModeCallback(const std_msgs::Bool::ConstPtr& msg)
{
  emc_joy_mode_data = msg->data;
}

void usb4750::emcSlowCallback(const std_msgs::Bool::ConstPtr& msg)
{
  slow_flag = msg->data;
}

void usb4750::convBrakeCallback(const std_msgs::UInt8::ConstPtr& msg)
{
  convbrakeV_ = (uint32)msg->data;
}

/* --- not use ---
void usb4750::stp1BrakeCallback(const std_msgs::UInt8::ConstPtr& msg)
{
  stp1brakeV_ = (uint32)msg->data;
}

void usb4750::stp2BrakeCallback(const std_msgs::UInt8::ConstPtr& msg)
{
  stp2brakeV_ = (uint32)msg->data;
}
--- here ---- */

void usb4750::chargeCallback(const std_msgs::UInt8::ConstPtr& msg)
{
  chgV_ = (uint32)msg->data;
}

void usb4750::NavControlCallback(
    const geometry_msgs::Twist::ConstPtr& motorCmd_)
{
  if ((motorCmd_->linear.x == 0) && (motorCmd_->linear.y == 0) &&
      (motorCmd_->angular.z == 0))
    agvAutoRun_ = false;
  else
    agvAutoRun_ = true;
}

void usb4750::JoyControlCallback(
    const move_control::teleop::ConstPtr& motorCmd_)
{
  if (motorCmd_->control_mode == 1)
    agvManuRun_ = true;
  else
    agvManuRun_ = false;
}

usb4750::~usb4750()
{
  instantDiCtrl->Dispose();
  instantDoCtrl->Dispose();
}

void usb4750::dIPub()
{

  usb_dio::io_in ioSig;

  ioSig.bump = bump_;
  ioSig.emc = emc_;
  ioSig.pcOff = pcOff_;

  pubDI.publish(ioSig);
}

void usb4750::dOPub()
{

  usb_dio::io_out outSig;

  outSig.chg = chgV_;
  outSig.front_pw = front_pwV_;
  outSig.wheel_pw = rear_pwV_;
  outSig.conv_pw = conv_pwV_;
  //outSig.stp1_cw = stp1CwV_;
  //outSig.stp2_cw = stp2CwV_;
  //outSig.stp1_ccw = stp1CcwV_;
  //outSig.stp2_ccw = stp2CcwV_;
  //outSig.stp1_brake = stp1brakeV_;
  //outSig.stp2_brake = stp2brakeV_;
  outSig.conv_brake = convbrakeV_;
  outSig.red = redV_;
  outSig.yel = yelV_;
  outSig.gre = greV_;
  outSig.buz = buzV_;
  outSig.bell = bellV_;

  pubDO.publish(outSig);
}

void usb4750::convLimPub()
{
  usb_dio::limConv lim_msgs;
  lim_msgs.in = conv_gatein_;
  lim_msgs.arrival = conv_arrival_;
  lim_msgs.mid = conv_mid_;

  publimConv.publish(lim_msgs);
}

/* -- not use ---
void usb4750::stp1LimPub()
{
  usb_dio::limStp lim_msgs;
  lim_msgs.up = stp1_up_;
  lim_msgs.down = stp1_down_;

  publimStp1.publish(lim_msgs);
}
void usb4750::stp2LimPub()
{
  usb_dio::limStp lim_msgs;
  lim_msgs.up = stp2_up_;
  lim_msgs.down = stp2_down_;

  publimStp2.publish(lim_msgs);
}

void usb4750::stp1cmdCallback(const std_msgs::Int8::ConstPtr& msg)
{
  if (msg->data == 1)
  {
    stp1CwV_ = 1;
    stp1CcwV_ = 0;
  }
  else if (msg->data == -1)
  {
    stp1CwV_ = 0;
    stp1CcwV_ = 1;
  }
  else
  {
    stp1CwV_ = 0;
    stp1CcwV_ = 0;
  }
}

void usb4750::stp2cmdCallback(const std_msgs::Int8::ConstPtr& msg)
{
  if (msg->data == 1)
  {
    stp2CwV_ = 1;
    stp2CcwV_ = 0;
  }
  else if (msg->data == -1)
  {
    stp2CwV_ = 0;
    stp2CcwV_ = 1;
  }
  else
  {
    stp2CwV_ = 0;
    stp2CcwV_ = 0;
  }
}

---- -here ---- */

int usb4750::port(int val)
{
  if (val < 8)
    return val;
  else if (val < 16)
    return val - 8;
  else if (val < 24)
    return val - 16;
  else
    return val - 24;
}
int usb4750::portNum(int val)
{
  if (val < 8)
    return 0;
  else if (val < 16)
    return 1;
  else if (val < 24)
    return 2;
  else
    return 3;
}

void usb4750::driverOn(int val)
{
  conv_pwV_ = val;
  front_pwV_ = val;
  rear_pwV_ = val;
}

int usb4750::spin()
{

  ros::Rate loop_rate(rate_);

  while (ros::ok()) // continouslly receive signals from a serial port
  {
    time_t curTime = time(NULL);
    struct tm *pLocal = localtime(&curTime);
    int now_hour = pLocal->tm_hour;

    if (pre_hour != now_hour) {
      // 시간이 같지 않을 경우
      int now_second = pLocal->tm_sec;
      if (chg_turn == true && now_second >= 5) {
        // 충전을 껏다 키기 5초가 지난 경우
        chg_turn = false;
      } else if (chg_turn == false) {
        // 충전을 껏다 키기 시작한 경우
        chg_turn = true;
      }
    }
    pre_hour = now_hour;

    byte bufferForReading[2] = {
        0}; // the first element of this array is used for start port
    ret = instantDiCtrl->Read(startPort, portCount, bufferForReading);

    // CHK_RESULT(ret);

    for (int i = 0; i < 8; i++)
    {
      DIs_[i] = ((bufferForReading[startPort] >> i) & 0x01);
    }

    for (int i = 0; i < 8; i++)
    {
      DIs_[i + 8] = ((bufferForReading[startPort + 1] >> i) & 0x01);
    }

    emc_ = DIs_[emcI_];
    pcOff_ = 1-DIs_[pcOffI_];
    bump_ = DIs_[bumpI_];
    dIPub();

    // 변수명 / msgs 이름 : 설명
    // gatein / in : 투입 감지
    // mid / mid : 제품 감지
    // arrival / arrival : 도착 감지
    conv_gatein_ = 1-DIs_[convInI_];
    conv_mid_ = 1-DIs_[convMidI_];
    conv_arrival_ = 1-DIs_[convArrivalI_];
    convLimPub();

    //pc off
    if(pcOff_ == 1)
    {
       if(pc_off_count >= 10)
       {
         system("shutdown -P now");
       }
       pc_off_count++; 
    }
    else
    {
        pc_off_count = 0;
    }

    //stp1_up_ = DIs_[stp1UpI_];
    //stp1_down_ = DIs_[stp1DownI_];
    //stp1LimPub();

    //stp2_up_ = DIs_[stp2UpI_];
    //stp2_down_ = DIs_[stp2DownI_];
    //stp2LimPub();

    if (emc_mode_data == true || emc_joy_mode_data == true) {
      // 비상 모드일 경우
      driverOn(1);
      redV_ = 1;
      greV_ = 1;
      yelV_ = 1;
      buzV_ = 1;
      bellV_ = 1; 
      count_ = 0;
    } else if ((emc_ == 1) || (bump_ == 1)) {
      // 물리적 비상 정지
      driverOn(0);
      redV_ = 1;
      greV_ = 0;
      yelV_ = 0;
      buzV_ = 1;
      bellV_ = 0; 
      count_ = 0;
    } else {
      if (agvAutoRun_ || convbrakeV_ == 1 || agvManuRun_)
      { 
        if (emc_state_data == true) {
          // 비상 정지
          driverOn(0);
          redV_ = 0;
          greV_ = 0;
          yelV_ = 0;
          buzV_ = 1;
          bellV_ = 0; 

          if (count_ <= 10) {
            redV_ = 1;
          }

          count_++;
          if (count_ >= 20)
            count_ = 0;

        } else {
          // 작동 작동 상태
          driverOn(1);
          yelV_ = 0;
          redV_ = 0;
          greV_ = 1;
          buzV_ = 0;
          bellV_ = 1; //1

          // 슬로우 상태일 경우
          if (slow_flag == true) {
            if (count_ <= 10) {
              yelV_ = 1;
            }

            count_++;
            if (count_ >= 20)
              count_ = 0;
          }
        }
      } else {
        // 대기 상태
        driverOn(0);
        greV_ = 0;
        yelV_ = 1;
        redV_ = 0;
        buzV_ = 0;
        bellV_ = 0;
        count_ = 0;
      }
    }

    ret2 = instantDoCtrl->WriteBit(startPort + portNum(redO_), port(redO_), redV_);
    ret2 = instantDoCtrl->WriteBit(startPort + portNum(buzO_), port(buzO_), buzV_);
    ret2 = instantDoCtrl->WriteBit(startPort + portNum(greO_), port(greO_), greV_);
    ret2 = instantDoCtrl->WriteBit(startPort + portNum(yelO_), port(yelO_), yelV_);
    ret2 = instantDoCtrl->WriteBit(startPort + portNum(bellO_), port(bellO_), bellV_);
    ret2 = instantDoCtrl->WriteBit(startPort + portNum(front_pwO_),
                                   port(front_pwO_), front_pwV_);
    ret2 = instantDoCtrl->WriteBit(startPort + portNum(rear_pwO_),
                                   port(rear_pwO_), rear_pwV_);
    ret2 = instantDoCtrl->WriteBit(startPort + portNum(conv_pwO_),
                                   port(conv_pwO_), conv_pwV_);
    ret2 = instantDoCtrl->WriteBit(startPort + portNum(convBrakeO_),
                                   port(convBrakeO_), convbrakeV_);
    //ret2 = instantDoCtrl->WriteBit(startPort + portNum(stp1BrakeO_),
    //                               port(stp1BrakeO_), stp1brakeV_);
    //ret2 = instantDoCtrl->WriteBit(startPort + portNum(stp1CwO_),
    //                               port(stp1CwO_), stp1CwV_);
    //ret2 = instantDoCtrl->WriteBit(startPort + portNum(stp1CcwO_),
    //                               port(stp1CcwO_), stp1CcwV_);
    //ret2 = instantDoCtrl->WriteBit(startPort + portNum(stp2BrakeO_),
    //                               port(stp2BrakeO_), stp2brakeV_);
    //ret2 = instantDoCtrl->WriteBit(startPort + portNum(stp2CwO_),
    //                               port(stp2CwO_), stp2CwV_);
    //ret2 = instantDoCtrl->WriteBit(startPort + portNum(stp2CcwO_),
    //                               port(stp2CcwO_), stp2CcwV_);
    if (chg_turn == true) {
      // 충전을 껏다 키고 있을 경우
      ret2 = instantDoCtrl->WriteBit(startPort + portNum(chgO_), port(chgO_), 0);
    } else {
      ret2 = instantDoCtrl->WriteBit(startPort + portNum(chgO_), port(chgO_), chgV_);
    }


    dOPub();

    // 연결 중 보내기
    connectPub(true);

    ros::spinOnce();
    loop_rate.sleep();
  }

  // 연결 해제 보내기
  usleep(100000);
  connectPub(false);
  usleep(100000);

  ret2 = instantDoCtrl->WriteBit(startPort + portNum(redO_), port(redO_), 0);
  ret2 = instantDoCtrl->WriteBit(startPort + portNum(buzO_), port(buzO_), 0);
  ret2 = instantDoCtrl->WriteBit(startPort + portNum(greO_), port(greO_), 0);
  ret2 = instantDoCtrl->WriteBit(startPort + portNum(yelO_), port(yelO_), 0);
  ret2 = instantDoCtrl->WriteBit(startPort + portNum(bellO_), port(bellO_), 0);
  ret2 = instantDoCtrl->WriteBit(startPort + portNum(front_pwO_),
                                 port(front_pwO_), 0);
  ret2 = instantDoCtrl->WriteBit(startPort + portNum(rear_pwO_),
                                 port(rear_pwO_), 0);
  ret2 = instantDoCtrl->WriteBit(startPort + portNum(conv_pwO_),
                                 port(conv_pwO_), 0);
  ret2 = instantDoCtrl->WriteBit(startPort + portNum(convBrakeO_),
                                 port(convBrakeO_), 0);
  //ret2 = instantDoCtrl->WriteBit(startPort + portNum(stp1BrakeO_),
  //                               port(stp1BrakeO_), 0);
  //ret2 = instantDoCtrl->WriteBit(startPort + portNum(stp1CwO_),
  //                               port(stp1CwO_), 0);
  //ret2 = instantDoCtrl->WriteBit(startPort + portNum(stp1CcwO_),
  //                               port(stp1CcwO_), 0);
  //ret2 = instantDoCtrl->WriteBit(startPort + portNum(stp2BrakeO_),
  //                               port(stp2BrakeO_), 0);
  //ret2 = instantDoCtrl->WriteBit(startPort + portNum(stp2CwO_),
  //                               port(stp2CwO_), 0);
  //ret2 = instantDoCtrl->WriteBit(startPort + portNum(stp2CcwO_),
  //                               port(stp2CcwO_), 0);
  ret2 = instantDoCtrl->WriteBit(startPort + portNum(chgO_),
                                 port(chgO_), 0);

  instantDiCtrl->Dispose();
  instantDoCtrl->Dispose();

  // If something wrong in this execution, print the error code on screen for
  // tracking.
  if (BioFailed(ret))
  {
    wchar_t enumString[256];
    AdxEnumToString(L"ErrorCode", (int32)ret, 256, enumString);
    printf("Some error occurred. And the last error code is 0x%X. [%ls]\n", ret,
           enumString);
  }

  return 0;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "~");
  usb4750 usbIO;
  return usbIO.spin();
}
