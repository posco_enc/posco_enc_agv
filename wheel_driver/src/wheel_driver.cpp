#include <wheel_driver.h>

void mySigintHandler(int sig)
{
  ROS_INFO("Received SIGINT signal, shutting down...");
  ros::shutdown();
}

WheelDriver::WheelDriver()
: data_idx(0), data_encoder_toss(5)
{
  // CBA Read local params (from launch file)
  ros::NodeHandle nhLocal("~");

  nhLocal.param<std::string>("cmdvel_topic", cmdvel_topic, "cmd");
  nhLocal.param<std::string>("port", port, "/dev/ttyUSB0");
  nhLocal.param("baud", baud, 115200);
  nhLocal.param("open_loop", open_loop, false);
  nhLocal.param("encoder_ppr", encoder_ppr, 3000);
  nhLocal.param("encoder_cpr", encoder_cpr, 12000);
  nhLocal.param("max_rpm", max_rpm, 2000.0);
  nhLocal.param("dir", _dir, 0);

  cmdvel_sub =
      nh.subscribe(cmdvel_topic, 10, &WheelDriver::cmdvel_callback, this);
  encoder_data =
      nh.advertise<motor_driver_msgs::Channel_value>("/encoder_data", 10);
  speed_data =
      nh.advertise<motor_driver_msgs::Channel_value>("/speed_data", 10);
  status_pub = nh.advertise<motor_driver_msgs::status>("/status", 10);
  connect_pub = nh.advertise<std_msgs::Bool>("/CONNECT", 10);
  bat_pub = nh.advertise<std_msgs::Int32MultiArray>("/bat", 10);
  sampling_time =
      nh.createTimer(ros::Duration(0.01), &WheelDriver::timer_callback, this);
}

//
// cmd_vel subscriber
//

void WheelDriver::cmdvel_callback(const motor_driver_msgs::Command& command)
{

  if (command.mode == motor_driver_msgs::Command::MODE_VELOCITY)
  {
    // wheel speed (m/s)
    _motor_speed = command.speed / max_rpm * 1000;
    motor_mode = 1;

    // endif open-loop
  }
  else // STOP MODE
  {
    _motor_speed = 0;
    motor_mode = 0;

  } // endif command.mode
}

void WheelDriver::timer_callback(const ros::TimerEvent& e)
{

  if (motor_mode == 1)
  {
    if (open_loop)
    {
      // motor power (scale 0-1000)

      if (_connect)
      {
        int32_t _power = static_cast<int>(_motor_speed * 1);
        std::stringstream _cmd;
        _cmd << "!G 1 " << _power << "\r";
        controller.write(_cmd.str());
        controller.flush();
      }
    }
    else
    {
      // motor speed (rpm)

      if (_connect)
      {
        int32_t _power = static_cast<int>(_motor_speed * 1);
        std::stringstream _cmd;
        _cmd << "!S 1 " << _power << "\r";
        controller.write(_cmd.str());
        controller.flush();
      }
    }
  }
  else
  {
    if (_connect)
    {
      int32_t _power = 0;
      std::stringstream _cmd;
      _cmd << "!S 1 " << _power << "\r";
      controller.write(_cmd.str());
      controller.flush();
    }
  }
}

void WheelDriver::driver_configure()
{

  // stop motors
  controller.write("!G 1 0\r");
  controller.write("!G 2 0\r");
  controller.write("!S 1 0\r");
  controller.write("!S 2 0\r");
  controller.flush();

  // disable echo
  controller.write("^ECHOF 1\r");
  controller.flush();

  // enable watchdog timer (1000 ms)
  controller.write("^RWD 250\r");
  //  controller.write("^RWD 250\r");

  // set motor operating mode (1 for closed-loop speed)
  if (open_loop)
  {
    controller.write("^MMOD 1 0\r");
  }
  else
  {
    controller.write("^MMOD 1 1\r");
  }

  // set motor amps limit (20 A * 10)
  controller.write("^ALIM 1 500\r");
  controller.write("^ALIM 2 500\r");

  // set max speed (rpm) for relative speed commands
  //  controller.write("^MXRPM 1 82\r");
  //  controller.write("^MXRPM 2 82\r");
  controller.write("^MXRPM 1 2000\r");
  controller.write("^MXRPM 2 2000\r");

  // set max acceleration rate (500 rpm/s * 10)
  //  controller.write("^MAC 1 2000\r");
  //  controller.write("^MAC 2 2000\r");
  controller.write("^MAC 1 5000\r");
  controller.write("^MAC 2 5000\r");

  // set max deceleration rate (2000 rpm/s * 10)
  controller.write("^MDEC 1 20000\r");
  controller.write("^MDEC 2 20000\r");

  // set number of poles
  controller.write("^BPOL 1 2\r");
  controller.write("^BPOL 2 2\r");

  // set PID parameters (gain * 10)
  controller.write("^KP 1 0\r");
  controller.write("^KP 2 0\r");
  controller.write("^KI 1 50\r");
  controller.write("^KI 2 50\r");
  controller.write("^KD 1 0\r");
  controller.write("^KD 2 0\r");

  // set encoder mode (18 for feedback on motor1, 34 for feedback on motor2)
  controller.write("^EMOD 1 18\r");
  controller.write("^EMOD 2 34\r");

  // set motor direction
  if (_dir == 1)
  {
    controller.write("^MDIR 1 1\r");
    controller.write("^MDIR 2 1\r");
  }
  else
  {
    controller.write("^MDIR 1 0\r");
    controller.write("^MDIR 2 0\r");
  }

  // set encoder counts (ppr)
  std::stringstream _enccmd;
  _enccmd << "^EPPR 1 " << encoder_ppr << "\r";
  controller.write(_enccmd.str());
  controller.flush();
}

void WheelDriver::data_query()
{
  // controller.write("# C_?CR_?BA_?V_?S_# 10\r");
  // controller.write("# C_?C_?S_# 10\r");

  controller.write("# C_?C_?S_?BA_?V_?FF_?FS_?HS_# 20\r");
  controller.flush();
}

void WheelDriver::encoder_publish()
{
  motor_driver_msgs::Channel_value encoder_value;
  encoder_value.header.stamp = ros::Time::now();
  encoder_value.data = _encoder;

  encoder_data.publish(encoder_value);
}

void WheelDriver::speed_publish()
{
  motor_driver_msgs::Channel_value speed_value;
  speed_value.header.stamp = ros::Time::now();
  speed_value.data = _rpm;

  speed_data.publish(speed_value);
}

void WheelDriver::bat_publish(int32_t volt_data, int32_t current_data) {
  std_msgs::Int32MultiArray bat_data;
  bat_data.data.push_back(volt_data);
  bat_data.data.push_back(current_data);
  bat_pub.publish(bat_data);
}

void WheelDriver::connect_publish(bool flag) {
  std_msgs::Bool connect_msgs;
  connect_msgs.data = flag;
  connect_pub.publish(connect_msgs);
}

void WheelDriver::data_parsing()
{

  // read sensor data stream from motor controller
  if (controller.available())
  {
    connect_publish(true);
    char ch = 0;
    if (controller.read((uint8_t*)&ch, 1) == 0)
      return;
    if (ch == '\r')
    {
      data_buf[data_idx] = 0;

      // Absolute encoder value C

      if (data_buf[0] == 'C' && data_buf[1] == '=')
      {
        int delim;
        for (delim = 2; delim < data_idx; delim++)
        {
          if (data_encoder_toss > 0)
          {
            --data_encoder_toss;
            break;
          }
          if (data_buf[delim] == ':')
          {
            data_buf[delim] = 0;
            _encoder = (int32_t)strtol(data_buf + 2, NULL, 10);
            //_encoder = (int32_t)strtol(data_buf + delim + 1, NULL, 10);
            encoder_publish();
            break;
          }
        }
      }
      // S= is speed in RPM
      if (data_buf[0] == 'S' && data_buf[1] == '=')
      {
        int delim;
        for (delim = 2; delim < data_idx; delim++)
        {
          if (data_buf[delim] == ':')
          {
            data_buf[delim] = 0;
            _rpm = (int32_t)strtol(data_buf + 2, NULL, 10);
            //_rpm = (int32_t)strtol(data_buf + delim + 1, NULL, 10);
            speed_publish();
            break;
          }
        }
      }

      // Voltage
      if (data_buf[0] == 'V' && data_buf[1] == '=')
      {
        int delim;
        for (delim = 2; delim < data_idx; delim++)
        {
          if (data_buf[delim] == ':')
          {
            data_buf[delim] = 0;
            _internal_vol = (int32_t)strtol(data_buf + 2, NULL, 10);
            _bat_vol = (int32_t)strtol(data_buf + delim + 1, NULL, 10);
            break;
          }
        }
      }

      // Current
      if (data_buf[0] == 'B' && data_buf[1] == 'A' && data_buf[2] == '=')
      {
        int delim;
        for (delim = 3; delim < data_idx; delim++)
        {
          if (data_buf[delim] == ':')
          {
            data_buf[delim] = 0;
            _current = (int32_t)strtol(data_buf + 3, NULL, 10);
            //_current = (int32_t)strtol(data_buf + delim + 1, NULL, 10);
            break;
          }
        }
      }

      bat_publish(_bat_vol, _current);

      // Fault
      if (data_buf[0] == 'F' && data_buf[1] == 'F' && data_buf[2] == '=')
      {
        _fault_flag = (int32_t)strtol(data_buf + 3, NULL, 10);
      }
      // Status
      if (data_buf[0] == 'F' && data_buf[1] == 'S' && data_buf[2] == '=')
      {
        _status_flag = (int32_t)strtol(data_buf + 3, NULL, 10);
      }

      // Hall sensor
      if (data_buf[0] == 'H' && data_buf[1] == 'S' && data_buf[2] == '=')
      {
        int delim;
        for (delim = 3; delim < data_idx; delim++)
        {
          if (data_buf[delim] == ':')
          {
            data_buf[delim] = 0;
            _hall_status = (int32_t)strtol(data_buf + 3, NULL, 10);
            //_hall_status = (int32_t)strtol(data_buf + delim + 1, NULL, 10);
            break;
          }
        }
      }

      status_publish();

      data_idx = 0;
    }
    else if (data_idx < (sizeof(data_buf) - 1))
    {
      data_buf[data_idx++] = ch;
    }
  }
}

void WheelDriver::status_publish()
{
  motor_driver_msgs::status status_msgs;
  status_msgs.status = _status_flag;
  status_msgs.fault = _fault_flag;
  status_msgs.bat_vol = _bat_vol;
  status_msgs.internal_vol = _internal_vol;
  status_msgs.cur = _current;
  status_msgs.hall = _hall_status;
  status_pub.publish(status_msgs);
}

int WheelDriver::run()
{

  serial::Timeout timeout = serial::Timeout::simpleTimeout(1000);
  controller.setPort(port);
  controller.setBaudrate(baud);
  controller.setTimeout(timeout);

  while (ros::ok())
  {
    try
    {
      controller.open();
      if (controller.isOpen())
      {
        // ROS_INFO("Successfully opened serial port");
          _connect = true;
        break;
      }
    }
    catch (serial::IOException e)
    {
      ROS_WARN_STREAM("serial::IOException: " << e.what());
      _connect = false;
    }
    ROS_WARN("Failed to open serial port");
    connect_publish(false);
    sleep(5);
  }
  if (_connect)
  {
    driver_configure();
    data_query();
  }

   //ros::Rate loopRate(100);

  while (ros::ok())
  {
    if (_connect)
      data_parsing();

    ros::spinOnce();

     //loopRate.sleep();
  }

  usleep(100000);
  connect_publish(false);
  usleep(100000);

  if (controller.isOpen())
    controller.close();

  ROS_INFO("Exiting");

  return 0;
}

int main(int argc, char** argv)
{

  ros::init(argc, argv, "wheel_driver_node");

  WheelDriver node;

  return node.run();
}
