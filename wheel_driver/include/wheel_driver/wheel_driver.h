#include "ros/time.h"
#include <motor_driver_msgs/Channel_value.h>
#include <motor_driver_msgs/Command.h>
#include <motor_driver_msgs/status.h>
#include <ros/console.h>
#include <ros/ros.h>
#include <serial/serial.h>
#include <signal.h>
#include <sstream>
#include <string>
#include <std_msgs/Bool.h>
#include <std_msgs/Int32MultiArray.h>
#include <unistd.h>

class WheelDriver
{

public:
  WheelDriver();

public:
  //
  // cmd_vel subscriber
  //
  void cmdvel_callback(const motor_driver_msgs::Command& command);
  void driver_configure();

  void data_query();
  void data_parsing();
  void timer_callback(const ros::TimerEvent& e);

  void encoder_publish();
  void speed_publish();
  void status_publish();
  void bat_publish(int32_t volt_data, int32_t current_data);
  void connect_publish(bool flag);
  int run();

protected:
  ros::NodeHandle nh;

  serial::Serial controller;

  static int32_t to_rpm(int32_t x) { return int32_t(x); }

  //
  // cmd subscriber
  //
  ros::Subscriber cmdvel_sub;

  //
  // speed publisher
  //
  ros::Publisher encoder_data;
  ros::Publisher speed_data;
  ros::Publisher status_pub;
  ros::Publisher bat_pub;
  ros::Publisher connect_pub;
  ros::Timer sampling_time;

  // buffer for reading encoder counts
  int data_idx = 0;
  char data_buf[24] = {0,};

  int32_t _encoder=0;
  int32_t _rpm=0;

  // settings

  std::string cmdvel_topic="CMD";
  std::string port="ttyUSB0";
  int baud=115200;
  bool open_loop=false;
  int encoder_ppr=3000;
  int encoder_cpr=12000; // 3000*4
  double max_rpm = 2000;
  int data_encoder_toss = 0;

  float _motor_speed = 0;
  int32_t motor_mode = -1;

  int _dir = 0;
  int _bat_vol = 0, _current = 0, _internal_vol = 0;
  int _fault_flag = 0, _hall_status = 0, _status_flag = 0;
  bool _connect = false;
};



