#!/usr/bin/env python3.8

import rospy

from std_msgs.msg import Bool
from usb_dio.msg import io_in
from move_control.msg import teleop
from msgs_node.msg import cmdMove
from geometry_msgs.msg import Twist
from obstacle_detector.msg import obstacles_detect

class EmergencyNode:
    ioin_emergency_stop: bool = False            # 범퍼, 비상 정지 버튼 여부
    obstacle_detector_stop: bool = False         # Obstacle 감지 정지 여부 (LRF 감지)
    obstacle_detector_slow: bool = False         # Obstacle 감지 슬로우 여부 (LRF 감지)
    acs_emergency_stop: bool = False             # ACS 비상 정지 여부
    acs_emergency_mode: bool = False             # ACS 비상 모드 여부
    joy_emergency_stop: bool = False             # 조이스틱 비상 정지 여부
    joy_emergency_mode: bool = False             # 조이스틱 비상 모드 여부
    precision_mode: bool = False                 # 정밀 모드 여부
    auto_mode: bool = False                      # 자동 모드 여부
    menu_mode: bool = False                      # 메뉴얼 모드 여부

    usb4750_connect: bool = True            # USB4750 연결 여부
    nav350_connect: bool = True             # SICK 센서 연결 여부
    wheel_front_left_connect: bool = True   # 바퀴 앞 왼쪽 연결 여부
    wheel_front_right_connect: bool = True  # 바퀴 앞 오른쪽 연결 여부
    wheel_rear_left_connect: bool = True    # 바퀴 뒤 왼쪽 연결 여부
    wheel_rear_left_connect: bool = True    # 바퀴 뒤 오른쪽 연결 여부
    conv_connect: bool = True             # 컨베이어 연결 여부
    
    usb4750_connect_count: int = 0              # USB4750 연결 카운트
    nav350_connect_count: int = 0               # SICK 센서 연결 카운트
    wheel_front_left_connect_count: int = 0     # 바퀴 앞 왼쪽 연결 카운트
    wheel_front_right_connect_count: int = 0    # 바퀴 앞 오른쪽 연결 카운트
    wheel_rear_left_connect_count: int = 0      # 바퀴 뒤 왼쪽 연결 카운트
    wheel_rear_right_connect_count: int = 0     # 바퀴 뒤 오른쪽 연결 카운트
    conv_connect_count: int = 0                 # 컨베이어 연결 카운트

    state_pub: rospy.Publisher                   # 비상 정지 상태 Pulisher (False: 비상 정지 상태 X / True: 비상 정지 상태)
    stop_pub: rospy.Publisher                    # 비상 정지 Pulisher (False: 비상 정지 X / True: 비상 정지[모터 정지])
    slow_pub: rospy.Publisher                    # 슬로우 Pulisher (False: 슬로우 X / True: 슬로우 상태)
 
    emergency_state_data: Bool = Bool()          # 비상 정지 상태
    emergency_stop_data: Bool = Bool()           # 비상 정지 
    slow_data:Bool = Bool()                      # 슬로우

    def __init__(self):
        rospy.init_node('emergency_node')

        # Set Variable
        rate = rospy.Rate(10) #10hz

        # Set Publish
        self.state_pub = rospy.Publisher("/EMERGENCY/STATE", Bool, queue_size=10)
        self.stop_pub = rospy.Publisher("/EMERGENCY/STOP", Bool, queue_size=10)
        self.slow_pub = rospy.Publisher("/EMERGENCY/SLOW", Bool, queue_size=10)

        # Set Subscriber
        rospy.Subscriber("/io_in", io_in, self.ioinCallback)                                         # 물리적 비상 정지 여부
        rospy.Subscriber("/obstacle_info", obstacles_detect, self.obstacleDetectCallback)             # 물체 감지 여부
        rospy.Subscriber("/obstacle_info/slow", obstacles_detect, self.obstacleDetectSlowCallback)    # 슬로우 구간 물체 감지 여부

        rospy.Subscriber("/EMERGENCY/MODE", Bool, self.acsEmergencyModeCallback)            # ACS 비상 모드 여부
        rospy.Subscriber("/EMERGENCY/ACS", Bool, self.acsEmergencyStopCallback)             # ACS 비상 정지 여부
        rospy.Subscriber("/EMERGENCY/JOY/MODE", Bool, self.emergencyJoyModeCallback)     # 조이스틱 비상 모드 여부
        rospy.Subscriber("/EMERGENCY/JOY", Bool, self.emergencyJoyStopCallback)          # 조이스틱 비상 모드 여부
        rospy.Subscriber("/MOVE/PRECISE", Bool, self.movePreciseCallback)
        rospy.Subscriber("/cmd_vel_joy", teleop, self.joystickControlCallback)              # 메뉴얼 모드 및 속도
        rospy.Subscriber("/cmd_vel", Twist, self.cmdVelCallback)                           # 자동 모드 및 속도

        rospy.Subscriber("/NAV350/CONNECT", Bool, self.nav350ConnectCallback)                # NAV350 연결 여부
        rospy.Subscriber("/USB4750/CONNECT", Bool, self.usb4750ConnectCallback)              # USB4750 연결 여부
        rospy.Subscriber("/left_front/CONNECT", Bool, self.wheelFLConnectCallback)           # 앞 왼쪽 바퀴 연결 여부
        rospy.Subscriber("/right_front/CONNECT", Bool, self.wheelFRConnectCallback)          # 앞 오른쪽 바퀴 연결 여부
        rospy.Subscriber("/left_rear/CONNECT", Bool, self.wheelRLConnectCallback)            # 뒤 왼쪽 바퀴 연결 여부
        rospy.Subscriber("/right_rear/CONNECT", Bool, self.wheelRRConnectCallback)           # 뒤 오른쪽 바퀴 연결 여부
        rospy.Subscriber("/CONV/CONNECT", Bool, self.convConnectCallback)                    # 컨베이어 연결 여부

        # Loop
        while not rospy.is_shutdown():
            self.update()
            rate.sleep()

    # 범퍼, 비상 정지 버튼 상태 가져오기
    def ioinCallback(self, ininData):
        self.ioin_emergency_stop = True if ininData.bump == 1 or ininData.emc == 1 else False

    # ACS 비상 모드 여부
    def acsEmergencyModeCallback(self, acsModeData):
        self.acs_emergency_mode = acsModeData.data

    # ACS 비상 정지 여부
    def acsEmergencyStopCallback(self, acsData):
        self.acs_emergency_stop = acsData.data

    # 조이스틱 비상 정지 여부
    def emergencyJoyStopCallback(self, joyStopData):
        self.joy_emergency_mode = joyStopData.data

    # 조이스틱 비상 모드 여부
    def emergencyJoyModeCallback(self, joyModeData):
        self.joy_emergency_stop = joyModeData.data

    # 정밀모드 가져오기
    def movePreciseCallback(self, move_precise_data):
        self.precision_mode = move_precise_data.data

    # 메뉴얼 모드 및 속도
    def joystickControlCallback(self, joystickData):
        self.menu_mode = True if joystickData.control_mode == 1 else False

    # 자동 모드 및 속도
    # 자동모드의 속도가 모두 0일 경우 False, 그러지 않을 경우 True
    def cmdVelCallback(self, cmdVelData):
        self.auto_mode = True if cmdVelData.linear.x != 0 or cmdVelData.linear.y != 0 or cmdVelData.angular.z != 0 else False

    # NAV350 연결 여부 가져오기
    def nav350ConnectCallback(self, nav350_connect_data):
        if nav350_connect_data.data is True:
            # 연결이 되어있으면
            self.nav350_connect_count = 0
            self.nav350_connect = True
        else:
            # 연결이 되어있지 않다면
            if self.nav350_connect_count >= 5:
                # 카운트가 5 이상이면
                self.nav350_connect = False
            else:
                self.nav350_connect_count += 1

    # USB4750 연결 여부 가져오기
    def usb4750ConnectCallback(self, usb4750_connect_data):
        if usb4750_connect_data.data is True:
            # 연결이 되어있으면
            self.usb4750_connect_count = 0
            self.usb4750_connect = True
        else:
            # 연결이 되어있지 않다면
            if self.usb4750_connect_count >= 5:
                # 카운트가 5 이상이면
                self.usb4750_connect = False
            else:
                self.usb4750_connect_count += 1

    # 앞 왼쪽 바퀴 연결 여부 가져오기
    def wheelFLConnectCallback(self, wheel_FL_connect_data):
        if wheel_FL_connect_data.data is True:
            # 연결이 되어있으면
            self.wheel_front_left_connect_count = 0
            self.wheel_front_left_connect = True
        else:
            # 연결이 되어있지 않다면
            if self.wheel_front_left_connect_count >= 5:
                # 카운트가 5 이상이면
                self.wheel_front_left_connect = False
            else:
                self.wheel_front_left_connect_count += 1

    # 앞 오른쪽 바퀴 연결 여부 가져오기
    def wheelFRConnectCallback(self, wheel_FR_connect_data):
        if wheel_FR_connect_data.data is True:
            # 연결이 되어있으면
            self.wheel_front_right_connect_count = 0
            self.wheel_front_right_connect = True
        else:
            # 연결이 되어있지 않다면
            if self.wheel_front_right_connect_count >= 5:
                # 카운트가 5 이상이면
                self.wheel_front_right_connect = False
            else:
                self.wheel_front_right_connect_count += 1

    # 뒤 왼쪽 바퀴 연결 여부 가져오기
    def wheelRLConnectCallback(self, wheel_RL_connect_data):
        if wheel_RL_connect_data.data is True:
            # 연결이 되어있으면
            self.wheel_rear_left_connect_count = 0
            self.wheel_rear_left_connect = True
        else:
            # 연결이 되어있지 않다면
            if self.wheel_rear_left_connect_count >= 5:
                # 카운트가 5 이상이면
                self.wheel_rear_left_connect = False
            else:
                self.wheel_rear_left_connect_count += 1

    # 뒤 오른쪽 바퀴 연결 여부 가져오기
    def wheelRRConnectCallback(self, wheel_RR_connect_data):
        if wheel_RR_connect_data.data is True:
            # 연결이 되어있으면
            self.wheel_rear_right_connect_count = 0
            self.wheel_rear_right_connect = True
        else:
            # 연결이 되어있지 않다면
            if self.wheel_rear_right_connect_count >= 5:
                # 카운트가 5 이상이면
                self.wheel_rear_right_connect = False
            else:
                self.wheel_rear_right_connect_count += 1

    # 컨베이어 연결 여부 가져오기
    def convConnectCallback(self, conv_connect_data):
        if conv_connect_data.data is True:
            # 연결이 되어있으면
            self.conv_connect_count = 0
            self.conv_connect = True
        else:
            # 연결이 되어있지 않다면
            if self.conv_connect_count >= 5:
                # 카운트가 5 이상이면
                self.conv_connect = False
            else:
                self.conv_connect_count += 1

    # 물체 감지 여부
    def obstacleDetectCallback(self, obstacle_detect_data):
        self.obstacle_detector_stop = False if obstacle_detect_data.number == 0 else True

    # 슬로우 구간 물체 감지 여부
    def obstacleDetectSlowCallback(self, obstacle_detect_slow_data):
        self.obstacle_detector_slow = False if obstacle_detect_slow_data.number == 0 else True

    # Loop
    def update(self):
        if self.ioin_emergency_stop is True or self.acs_emergency_stop is True or self.joy_emergency_stop is True:
            # 범퍼, 비상 정지 버튼, ACS 비상 정지 True 라면
            # 비상 정지
            self.emergency_state_data.data = True
        elif self.menu_mode is False and self.auto_mode is False:
            # 동작 중이지 않다면
            # 비상 정지 해제
            self.emergency_state_data.data = False
        elif self.menu_mode is True:
            # 수동모드라면 비상 정지 해제
            self.emergency_state_data.data = False
        elif self.nav350_connect is False or \
            self.usb4750_connect is False or \
            self.wheel_front_left_connect is False or \
            self.wheel_front_right_connect is False or \
            self.wheel_rear_left_connect is False or \
            self.wheel_rear_right_connect is False:
            # NAV350 또는 USB4750 또는 바퀴 연결이 해제되어있다면
            # 비상 정지
            self.emergency_state_data.data = True
        else:
            if self.precision_mode is False:
                # 정밀 모드가 아니라면
                if self.obstacle_detector_stop is True:
                    # 물체 감지 되었다면
                    # 비상 정지
                    self.emergency_state_data.data = True
                else:
                    # 물체 감지가 되지않았다면
                    # 비상 정지 해제
                    self.emergency_state_data.data = False
            else:
                # 정밀 모드라면
                # 비상 정지 해제
                self.emergency_state_data.data = False

        # 비상모드 활성화 시 (비상 상태에서 움직임)
        self.emergency_stop_data.data = False if self.acs_emergency_mode is True or self.joy_emergency_mode is True else self.emergency_state_data.data

        # 비상 정지 해제 상태, 슬로우 구간 물체 인식 시
        self.slow_data.data = True if self.emergency_stop_data.data is False and self.obstacle_detector_slow is True else False


        #
        #
        if self.menu_mode is True:
            self.slow_data.data = False

        # publish 하기
        self.state_pub.publish(self.emergency_state_data)
        self.stop_pub.publish(self.emergency_stop_data)
        self.slow_pub.publish(self.slow_data)


if __name__=='__main__':
    EmergencyNode()