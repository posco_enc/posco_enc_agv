#include "move_control/teleop.h"
#include "ros/time.h"
#include <move_control/convJoy.h>
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <std_msgs/Int8.h>
#include <std_msgs/Bool.h>

class TeleopMotor
{
public:
  TeleopMotor();

  int run();

private:
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);
  void joyConnectCallback(const std_msgs::Bool::ConstPtr& joyConnect);
  int sgn(double speed);
  void publish_motor_command(double velx, double vely, double angz);
  void pub_conv_pub(double conv);
  ros::Time joy_header;

  ros::NodeHandle nh_;

  int linear_x, linear_y, angular_z, conv_ctrl;
  int move_btn, auto_btn, mode_btn, turbo_btn, slow_btn, conv_btn;
  double conv_spd_scale, slow_scale,fast_scale;
  int move_mode, conv_mode = 0;
  double vel_x, vel_y, ang_z;
  //int stp1_axis = 7, stp2_axis = 6;
  //int stp1_sig = 0, stp2_sig = 0;
  double conv;
  ros::Publisher vel_pub_, conv_pub, stp1_pub, stp2_pub;
  ros::Subscriber joy_sub_, joy_connect_sub_;

  int rate_ = 10; //20
  double joy_scale = 0.5;
  bool joy_connect_ = false;
};

//    : linear_x(1), linear_y(0), angular_z(3), conv_ctrl(4), enable_btn(1),
//      lock_btn(0), mode_btn(5), turbo_btn(4), slow_btn(6), conv_btn(3),
//      conv_spd_scale(1.0),
//      fast_scale(1.2), slow_scale(0.5), move_mode(-1)

TeleopMotor::TeleopMotor()
{
  // axis
  nh_.param("axis_linear_x", linear_x, linear_x);
  nh_.param("axis_linear_y", linear_y, linear_y);
  nh_.param("axis_angular", angular_z, angular_z);
  nh_.param("axis_conv", conv_ctrl, conv_ctrl);
  
  // op.
  nh_.param("mode_button", mode_btn, mode_btn);
  nh_.param("move_button", move_btn, move_btn);
  nh_.param("auto_button", auto_btn, auto_btn);
  nh_.param("conv_button", conv_btn, conv_btn);
  nh_.param("turbo_button", turbo_btn, turbo_btn);
  nh_.param("slow_button", slow_btn, slow_btn);
//  nh_.param("stp1_axis", stp1_axis, stp1_axis); // not use.
//  nh_.param("stp2_axis", stp2_axis, stp2_axis); // not use
  nh_.param("conv_spd_scale", conv_spd_scale, conv_spd_scale);
  nh_.param("fast_scale", fast_scale, fast_scale);
  nh_.param("slow_scale", slow_scale, slow_scale);
  nh_.param("joy_scale", joy_scale, joy_scale);

  vel_pub_ = nh_.advertise<move_control::teleop>("/cmd_vel_joy", 1);
  joy_sub_ = nh_.subscribe<sensor_msgs::Joy>("/joy", 1, &TeleopMotor::joyCallback, this);
  joy_connect_sub_ = nh_.subscribe<std_msgs::Bool>("/joy/CONNECT", 1, &TeleopMotor::joyConnectCallback, this);
  conv_pub = nh_.advertise<move_control::convJoy>("/CONV/MAN", 1);

  //stp1_pub = nh_.advertise<std_msgs::Int8>("/STP1/MAN", 1);
  //stp2_pub = nh_.advertise<std_msgs::Int8>("/STP2/MAN", 1);

  move_mode = -1;
  vel_x = 0, vel_y = 0, ang_z = 0;
  conv = 0;
}

void TeleopMotor::joyConnectCallback(const std_msgs::Bool::ConstPtr& joyConnect)
{
  joy_connect_ = joyConnect->data;
}

void TeleopMotor::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  joy_header = joy->header.stamp;
  if (joy->buttons[mode_btn] == 1 && joy->buttons[move_btn] == 1)
  {
    move_mode = 1;
    conv_mode = 0;
  }
  if (joy->buttons[mode_btn] == 1 && joy->buttons[auto_btn] == 1)
  {
    move_mode = 0;
    conv_mode = 0;
  }

  if (joy->buttons[mode_btn] == 1 && joy->buttons[conv_btn] == 1)
  {
    conv_mode = 1;
    move_mode = 0;
  }
  /////////////////////////////////////////////////////////////////////////////////////
  ///                                                                               ///
  ///                               Wheel Motor Setting ///
  ///                                                                               ///
  /////////////////////////////////////////////////////////////////////////////////////
  if (move_mode == 1)
  {
    double lin_spd_x = 0, lin_spd_y = 0, rot_spd_z = 0;
    int dir_x = 0, dir_y = 0, dir_z = 0;

    if (fabs(joy->buttons[turbo_btn]) > 0.8 && fabs(joy->buttons[slow_btn]) < 0.2)
    {// turbo mode

      if (fabs(joy->axes[linear_x]) > 0.15 && fabs(joy->axes[linear_y]) < 0.15)
      {
        lin_spd_x = double( fabs(joy->axes[linear_x]) * fast_scale );
        lin_spd_y = 0;
      }
      else
      {
        if (fabs(joy->axes[linear_x]) < 0.15 && fabs(joy->axes[linear_y]) > 0.15)
        {
          lin_spd_x = 0;
          lin_spd_y = double(fabs(joy->axes[linear_y]) * fast_scale);
        }
        else
        {
          if (fabs(joy->axes[linear_x]) > 0.15 && fabs(joy->axes[linear_y]) > 0.15)
          {
            lin_spd_x = double(fabs(joy->axes[linear_x]) * fast_scale);
            lin_spd_y = double(fabs(joy->axes[linear_y]) * fast_scale);
          }
          else
          {
            lin_spd_x = 0;
            lin_spd_y = 0;
          }
        }
      }
      rot_spd_z = double(fabs(joy->axes[angular_z]) * fast_scale)*0.5;
    }
    else if (fabs(joy->buttons[slow_btn]) > 0.8)
    { // slow mode
      if (fabs(joy->axes[linear_x]) > 0.15 && fabs(joy->axes[linear_y]) < 0.15)
      {
        lin_spd_x = double(fabs(joy->axes[linear_x]) * slow_scale);
        lin_spd_y = 0;
      }
      else
      {
        if (fabs(joy->axes[linear_x]) < 0.15 && fabs(joy->axes[linear_y]) > 0.15)
        {
          lin_spd_x = 0;
          lin_spd_y = double(fabs(joy->axes[linear_y]) * slow_scale);
        }
        else
        {
          if (fabs(joy->axes[linear_x]) > 0.15 && fabs(joy->axes[linear_y]) > 0.15)
          {
            lin_spd_x = (fabs(joy->axes[linear_x]) * slow_scale);
            lin_spd_y = (fabs(joy->axes[linear_y]) * slow_scale);
          }
          else
          {
            lin_spd_x = 0;
            lin_spd_y = 0;
          }
        }
      }
      rot_spd_z = double(fabs(joy->axes[angular_z]) * slow_scale);
    }
    else
    { // normal mode
      if (fabs(joy->axes[linear_x]) > 0.15 && fabs(joy->axes[linear_y]) < 0.15)
      {
        lin_spd_x = double(fabs(joy->axes[linear_x]) );
        lin_spd_y = 0;
      }
      else
      {
        if (fabs(joy->axes[linear_x]) < 0.15 && fabs(joy->axes[linear_y]) > 0.15)
        {
          lin_spd_x = 0;
          lin_spd_y = double(fabs(joy->axes[linear_y]) );
        }
        else
        {
          if (fabs(joy->axes[linear_x]) > 0.15 && fabs(joy->axes[linear_y]) > 0.15)
          {
            lin_spd_x = double(fabs(joy->axes[linear_x]) );
            lin_spd_y = double(fabs(joy->axes[linear_y]) );
          }
          else
          {
            lin_spd_x = 0;
            lin_spd_y = 0;
          }
        }
      }
      rot_spd_z = double(fabs(joy->axes[angular_z])) * 0.5;
    } /// speed mode

    dir_x = sgn(joy->axes[linear_x]);
    dir_y = sgn(joy->axes[linear_y]);
    dir_z = sgn(joy->axes[angular_z]);

    vel_x = double(lin_spd_x * dir_x * joy_scale);
    vel_y = double(lin_spd_y * dir_y * joy_scale);
    ang_z = double(rot_spd_z * dir_z * joy_scale);
  }
  else
  {
    vel_x = 0, vel_y = 0, ang_z = 0;
  }

  //////////////////////////////////////////////////////////////////////////////////////
  ///                                                                                ///
  ///                                 conveyor setting ///
  ///                                                                                ///
  //////////////////////////////////////////////////////////////////////////////////////
  if (conv_mode == 1)
  {
    double conv_spd = 0;
    int conv_dir = 0;

    if (fabs(joy->axes[conv_ctrl]) > 0.15)
    {
      if (fabs(joy->buttons[turbo_btn]) > 0.8)
      {
        conv_spd = double( fabs(joy->axes[conv_ctrl]) * fast_scale);
        // ROS_INFO("fabs -> y_ctrl %f", fabs(joy->axes[y_ctrl]));
        // ROS_INFO("conv_spd -> %f",conv_spd);
      }
      else if (fabs(joy->buttons[slow_btn]) > 0.8)
      {
        conv_spd = double( fabs(joy->axes[conv_ctrl]) * slow_scale);
        // ROS_INFO("fabs -> y_ctrl %f", fabs(joy->axes[y_ctrl]));
        // ROS_INFO("conv_spd -> %f",conv_spd);
      }
      else
      {
        conv_spd = double( fabs(joy->axes[conv_ctrl]) );
        // ROS_INFO("fabs -> y_ctrl %f", fabs(joy->axes[y_ctrl]));
        // ROS_INFO("conv_spd -> %f",conv_spd);
      }
    }
    else
    {
      conv_spd = 0;
    }

    conv_dir = sgn(joy->axes[conv_ctrl]);
    conv = conv_spd * conv_dir * joy_scale ; //* 0.1;  // what?? 0.1
    // ROS_INFO("conv_dir -> %d", conv_dir);
    //ROS_INFO("conv_speed -> %f", conv);

    /* ----- not use
    if(joy->axes[stp1_axis] > 0.5)
    {
        stp1_sig = 1;
        std_msgs::Int8 stp1_msgs;
        stp1_msgs.data = stp1_sig;
        stp1_pub.publish(stp1_msgs);
    }
    else if(joy->axes[stp1_axis] < -0.5)
    {
        stp1_sig = -1;
        std_msgs::Int8 stp1_msgs;
        stp1_msgs.data = stp1_sig;
        stp1_pub.publish(stp1_msgs);
    }
    else
        stp1_sig = 0;
  ------------- here ------------*/
  }
  else
  {
    conv = 0;
  }

  /* ------------- not use ------------
  if(joy->axes[stp2_axis] > 0.5)
  {
      stp2_sig = 1;
      std_msgs::Int8 stp2_msgs;
      stp2_msgs.data = stp2_sig;
      stp2_pub.publish(stp2_msgs);
  }
  else if(joy->axes[stp2_axis] < -0.5)
  {
      stp2_sig = -1;
      std_msgs::Int8 stp2_msgs;
      stp2_msgs.data = stp2_sig;
      stp2_pub.publish(stp2_msgs);
  }
  else
      stp2_sig = 0
  ------------- here ------------*/

}

void TeleopMotor::publish_motor_command(double velx, double vely, double angz)
{
  move_control::teleop move_mecanum;

  move_mecanum.control_mode = 0;

  if (move_mode == 1) // VARIABLE SPEED MODE
  {
    conv_mode = 0;
    move_mecanum.control_mode = 1;
    move_mecanum.linear_x = velx;
    move_mecanum.linear_y = vely;
    move_mecanum.angular_z = angz;
  }
  if(conv_mode == 1)
  { 
    move_mode = 0;
    move_mecanum.control_mode = 1;
    move_mecanum.linear_x = 0;
    move_mecanum.linear_y = 0;
    move_mecanum.angular_z = 0;
  }
  vel_pub_.publish(move_mecanum);
}

void TeleopMotor::pub_conv_pub(double conv)
{
  move_control::convJoy conv_msgs;
  if (conv_mode == 1)
  {
    conv_msgs.speed = conv;
    conv_msgs.mode = 1;
  }
  else
  {
    conv_msgs.speed = 0;
    conv_msgs.mode = 0;
  }
  conv_pub.publish(conv_msgs);
}

int TeleopMotor::sgn(double speed)
{
  if (speed < 0)
  {
    return -1;
  }
  else if (speed > 0)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

int TeleopMotor::run()
{
  ros::Rate loop_rate(rate_);
  while (ros::ok())
  {
    ros::Time time = ros::Time::now();
    double dt = (time - joy_header).toSec();

    if (!joy_connect_) {
      // not connect (add 22.02.23)
      conv_mode = 0;
      move_mode = 0;
      vel_x = 0.0;
      vel_y = 0.0;
      ang_z = 0.0;
      conv = 0.0;
    }

    if (fabs(dt) < 1.1)
    {
      publish_motor_command(vel_x, vel_y, ang_z);
      pub_conv_pub(conv);
      ROS_INFO("conv -> %f", conv);
    }
    else
    {
      publish_motor_command(0, 0, 0);
      pub_conv_pub(0);
    }
    ros::spinOnce();
    loop_rate.sleep();
  }
  publish_motor_command(0, 0, 0);
  pub_conv_pub(0);
  return 0;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "ps4_agv");
  TeleopMotor ps4_agv;
  return ps4_agv.run();
  // ros::spin();
}
