#include <ros/ros.h>
#include <std_msgs/Int8.h>
#include "ros/time.h"
#include "usb_dio/io_in.h"
#include <QProcess>
#include <QDebug>

class Reset
{
public:
    Reset();
    ~Reset();

    bool init();
    void spin();

    void pwInCallback(const usb_dio::io_in::ConstPtr& msg);



private:
    ros::NodeHandle nh_;

    ros::Subscriber pgv_sub, subIO;
    int fault, line_id;

    int8_t old_msg;

    QProcess process, proc;
    bool pwSw_ = true, prePwSw_ = true;
    int32_t swCnt_ = 0;
    int16_t count_= 0;
};

Reset::Reset() : old_msg(0)
{
    if(init())
        spin();
}

Reset::~Reset()
{

}

bool Reset::init()
{
    if( !ros::master::check() )
        return false;

    subIO = nh_.subscribe("io_in",1, &Reset::pwInCallback, this);

    return true;
}


void Reset::pwInCallback(const usb_dio::io_in::ConstPtr& msg)
{
    pwSw_ = msg->pcOff;
    if(pwSw_ == false && prePwSw_ == false)
    {
        swCnt_ ++;
    }
    else
        swCnt_ = 0;
    prePwSw_ = pwSw_;
}

void Reset::spin()
{
    ros::Rate loop_rate(10);

    while(ros::ok())
    {
        if(swCnt_>30)
        {
            QStringList list;
            list << "/home/sis/sh/exit.sh";
            proc.execute("/bin/bash",list);

            proc.startDetached("/sbin/shutdown -h now");
            //ros::shutdown();
        }

        count_ ++;
        if(count_ == 1200)
        {
            count_ = 0;
            QProcess process;
            QStringList list;
            list << "/home/sis/sh/clean.sh";
            process.execute("/bin/bash",list);
        }

        ros::spinOnce();
        loop_rate.sleep();
    }
}


int main(int argc, char** argv)
{
    ros::init(argc, argv,"reset_node");

    Reset();

    return 0;
}


