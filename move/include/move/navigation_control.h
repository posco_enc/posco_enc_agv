#include "ros/time.h"
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Twist.h>
#include <msgs_node/cmdMove.h>
#include <msgs_node/waypoint.h>
#include <ros/ros.h>
#include <std_msgs/UInt8.h>
#include <tf/transform_broadcaster.h>
#include <vector>

#include <fstream>
#include <iostream>
#include <math.h>

#include "move_control/convCmd.h"
#include "move_control/convStatus.h"
#include "move_control/conv_act.h"
#include "move_control/conv_state.h"
#include "move_control/teleop.h"

#include "motor_driver_msgs/status.h"

#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf/LinearMath/Transform.h>
#include <tf2_ros/transform_listener.h>

#include <obstacle_detector/obstacles_detect.h>
#include <pgv/vision_msg.h>
#include <QProcess>

#define NO_WHERE 0
#define H_AGV 1
#define CHG 2
#define RB2 3
#define RB3 4
#define RB1 5
#define RA1 6
#define RA2 7
#define RA3 8
#define RA4 9
#define CONV 10
#define GND 11
#define BACK_GND 12

#define WP_CUR 11
#define WP_MID2 12
#define WP_GOAL 13
#define GOAL 14
#define WP_MID1 15
#define WP_MID3 16

#define NO_conv 0
#define W_OUT 1
#define Y_FORW 2
#define Y_BACKW 3
#define Z_DOWN 4
#define Z_H 5
#define Z_UP 6

#define F_HOME 0
#define LIM 1
#define BACK 2
#define MID 3

#define F_STP 0
#define F_MOV 1
#define F_GOAL 3
#define F_ER 2

#define T_IN 1
#define T_OUT 2
#define P_GND 3
#define L_GND 4

using namespace std;

namespace agv_wp
{

tf2_ros::Buffer tfBuffer;

enum MODE_AGV
{
  mode_stop_ = 1,
  mode_start_ = 2,
  mode_obs_ = 3,
  mode_emc_ = 4,
  mode_fail_ = 5,
  mode_recover_ = 6,
  mode_motor_er = 7
};

enum MODE_conv
{
  conv_stop_ = 1,
  conv_in = 2,
  conv_out = 3,
  conv_emc = 6,
  conv_gnd = 4,
  conv_lift_gnd = 5,
  conv_zhome = 7
};

enum WP
{
  NO_POINT_ = 0,
  INDIRECT_ = 1,
  DIRECT_ = 2
};

enum conv_STATUS
{
  conv_STP = 0,
  conv_MAN = 1,
  conv_MOV = 2,
  conv_GOAL = 3
};

struct conv_STATE
{
  double Dist = 0.0;
  int8_t Mode = 0;
  int8_t Pos = 0;
  int8_t Status = 0;
};

enum AGV_STATUS
{
  AGV_STP = 0,
  WP_REACH = 2,
  LRF_PREC = 4,
  GOAL_REACH = 3,
  LRF_RUN = 1,
  OBS = 5,
  MANUAL = 6,
  LRF_XY_COMP = 7,
  LRF_FAIL = 8,
  LRF_RECOVER = 9,
  QR_RUN = 10,
  QR_COMP = 11
};

enum MODE_MOVE
{
  MOVE_ZERO_ = 0,
  MOVE_LRF_ = 1,
  MOVE_QR_ = 2,
};

enum LRF_MOVE
{
  LRF_NONE_ = 0,
  // LRF_WP = 4,
  LRF_XY_ = 1,
  LRF_DIAG = 2,
  LRF_YAW_ = 3
};

int8_t move_point_;
MODE_AGV mode_agv_;
int8_t agvPos_;
int8_t agvGoal_;
int8_t agvSubGoal_;
AGV_STATUS agvStatus_;
MODE_MOVE moveMode_;
LRF_MOVE lrfMove_;

MODE_conv mode_conv_, conv_pre_;
conv_STATUS convStatus_;

conv_STATE yState, zState, wState;

int TAKE_IN[5] = {Z_H, Y_FORW, Z_DOWN, Y_BACKW, Z_H};
int TAKE_OUT[5] = {W_OUT, Z_DOWN, Y_FORW, Z_UP, Y_BACKW};
int PUT_GND[2] = {Y_FORW, Z_DOWN};
int LIFT_GND[2] = {Z_H, Y_BACKW};

int8_t conv_task_;

int PATH[6] = {WP_CUR, WP_MID1, WP_MID2, WP_MID3, WP_GOAL, GOAL};

vector<float> HAGV, CHARGE, MID_WP, RB1_WP, TRANS_WP;

class NavigationManagement
{
public:
  NavigationManagement();
  ~NavigationManagement();
  void init();
  void spin();
  void update();
  void param_set();

private:
  void nav_control();
  void XY_control();
  void diag_control();
  // void WP_control();
  void YAW_control();
  void qr_ctrl();
  double lrf_tracking_ctrl(double Kp, double dx, double vmax, double vmin);

  void goal_move();
  void wp_cur_move();
  void wp_goal_move();
  void WP_MID2_move();
  void WP_MID1_move();
  void WP_MID3_move();

  void y_forw();
  void y_backw();
  void z_down();
  void w_out();
  void z_h();
  void z_up();

  void stop_conv();
  void conv_pub();

  WP wp_;
  bool obstacle_;
  int rate = 10;
  int move_state; // move : 1, stop : 0
  float target_pos_x, target_pos_y, target_A;
  float goal_x, goal_y, goal_A;
  float current_x = 0, current_y = 0, current_A = 0;
  float cur_x = 0, cur_y = 0, cur_A = 0;
  bool manual_mode;

  double delX_TOL = 0.008, delY_TOL = 0.01, delTH_TOL = 0.2;
  double delX_LSTP = 0.3;
  double delY_LSTP = 0.3;
  double delX_ = 0, delY_ = 0, delTH_ = 0, lenX_ = 0, lenY_ = 0, angTH_ = 0;
  double delTH_LSTP = 10, delTH_STA = 10;
  double LVX_MAX = 0.35, LVY_MAX = 0.25, LVTH_MAX = 0.06;
  double ratio_ = 0;
  int8_t count, pt_, pts_, rec_cnt_ = 0;
  int8_t cntF_ = 0, pF_ = 0, pFs_ = 0;
  double Kp_ = 0.6;

  float old_x = 0, old_y = 0, old_A = 0, old_vx = 0, old_vy = 0, old_vA = 0;
  int init_cnt_ = 0;
  bool lrf_fail = false;
  double H1_X = 0.0, H1_Y = 0.0, H1_A = 0.0;

  double fyM_ = 1044.0;
  double fzM_ = -316.0;
  double fwM_ = 240.0;
  double target_hgt = 0.0, target_wid = 0.0, target_len = 0.0, z_goal = 0.0;

  uint8_t tag_, control_;
  int tag_id, fault, warning, command, control_id;
  double line_x, line_y, line_theta;

  double QR_VEL = 0.01, QR_AVEL = 0.006;

  bool obs_detect = false, emc_push_ = false;
  bool recover_ = false;
  int8_t _motor_status = 0;

  int8_t conv_mode_ = 0;

  uint8_t pgvStatus_ = 0, cnt_pgv = 0;

  std_msgs::UInt8 state_goal;
  geometry_msgs::Twist vel_;
  geometry_msgs::Pose2D cur_goal_;

  ros::NodeHandle nh;
  ros::Publisher goal_pub, waypoint_pub, vel_pub, emc_pub, pub_initPos, pub_FY,
      pub_FZ, pub_FW, pub_Fstate, pub_CHG;
  ros::Subscriber reach_sub, goal_sub, obstacle_sub, joy_sub, sub_init,
      sub_conv;
  ros::Subscriber sub_Ystatus, sub_Wstatus, sub_Zstatus;
  ros::Subscriber sub_motor_status;

  QProcess process;
  int8_t cnt_mot_er = 0, cnt_reset = 0;

  void obstacle_cb(const obstacle_detector::obstacles_detect msg);

  void goal_target_cb(const msgs_node::cmdMove& goal);
  void wp_pub();
  void joystickControlCallback(const move_control::teleop::ConstPtr& motorCmd_);
  void init_cb(const std_msgs::UInt8::ConstPtr& msg);
  void stop_agv();
  float sign(float in);
  void convActCallback(const move_control::conv_act::ConstPtr& convCmd_);
  void YstatusCallback(move_control::convStatus msg);
  void WstatusCallback(move_control::convStatus msg);
  void ZstatusCallback(move_control::convStatus msg);
  void pgv_cb(const pgv::vision_msg& pgv_msg);
  void pgv_status_cb(const std_msgs::UInt8& msg);
  void motor_status_cb(motor_driver_msgs::status msg);

  // void timer_callback(const ros::TimerEvent& e);
};

} // namespace agv_wp

using namespace std;
