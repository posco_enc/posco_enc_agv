#!/usr/bin/env python3.8
import rospy
import copy
from enum import Enum

from geometry_msgs.msg import Pose2D, Twist
from msgs_node.msg import cmdMove
from std_msgs.msg import UInt8, Bool
from move_control.msg import convJoy, convStatus, teleop
from usb_dio.msg import limConv


# move 종류
class MoveType(Enum):
    STOP = 0         # 정지
    XY_MOVE = 1      # X, Y축 이동
    ROTATE = 2       # 회전
    COV_MOVE = 3     # 컨베이어 작동
    GOAL_REACH = 4   # 목표지점 도달


# AGV 오토 동작 모드 종류
class AgvMode(Enum):
    STOP = 0        # 정지 상태
    START = 1       # 시작 상태
    ERROR = 2       # 에러 상태


# 속도 설정
# speed_scale : 속도 크기
# speed : 속도
# vmax : 최대 속도
# vmin : 최저 속도
def lrf_tracking_ctrl(speed_scale, speed, vmax, vmin):
    v_cal = speed_scale * speed
    if v_cal >= 0:
        # Forward
        if v_cal > vmax:
            # 최대 속도보다 크면 최고 속도로 설정
            v_cal = vmax
        if v_cal < vmin:
            # 최저 속도보다 작으면 최저 속도 설정
            v_cal = vmin
    else:
        # Backward
        if v_cal < -vmax:
            # 최대 속도보다 크면 최고 속도로 설정
            v_cal = -vmax
        if v_cal > -vmin:
            # 최저 속도보다 작으면 최저 속도 설정
            v_cal = -vmin
    return v_cal


class NavigationControl: 
    # ====================================================
    #                   상태 관련 정의
    # ====================================================
    manual_mode: bool = False   # 메뉴얼 모드 여부
    auto_move: AgvMode = AgvMode.STOP      # AGV 오토 동작 모드
    move_mode: MoveType = MoveType.STOP      # 오토모드 종류
    number_data: int = 0        # 오토모드 번호 (순서, 인덱스 용)

    precise_mode: Bool = Bool()       # 정밀모드 여부 (속도 느려짐)
    precise_mode.data = False      # 정밀모드 여부 (속도 느려짐, 근접센서 OFF)

    # conv_reach_count: int = 0      # 컨베이어 도달 카운트

    # ====================================================
    #         위치, 속도, 남은 거리 관련 정의
    # ====================================================
    current_pose: Pose2D = Pose2D()       # AGV 현재 위치
    current_pose.x = 0.0           # AGV 현재 X 축
    current_pose.y = 0.0           # AGV 현재 Y 축
    current_pose.theta = 0.0       # AGV 현재 방향

    target_pose: Pose2D = Pose2D()      # 목표 위치
    target_pose.x = 0.0                 # 타겟 X 축
    target_pose.y = 0.0                 # 타겟 Y 축
    target_pose.theta = 0.0             # 타겟 방향

    velocity: Twist = Twist()     # 속도 지정
    velocity.linear.x = 0.0       # 속도 X 축
    velocity.linear.y = 0.0       # 속도 Y 축
    velocity.linear.z = 0.0       # 속도 Z 축 (사용 X)
    velocity.angular.x = 0.0      # 속도 각도 X (사용 X)
    velocity.angular.y = 0.0      # 속도 각도 X (사용 X)
    velocity.angular.z = 0.0      # 타겟 방향

    conv_velocity: convJoy = convJoy()    # 컨베이어 속도 지정
    conv_velocity.mode = 0        # 컨베이어 속도 모드
    conv_velocity.speed = 0.0     # 컨베이어 속도

    conv_pose: float = 0.0                # 컨베이어 위치

    lenX: float = 0.0        # X 남은 거리
    lenY: float = 0.0        # Y 남은 거리
    angTH: float = 0.0       # 남은 각도

    # ====================================================
    #              속도, 거리 제한 관련 정의
    # ====================================================
    marginX: float = 0.02                 # X축 허용 값
    marginY: float = 0.02                 # Y축 허용 값
    marginTH: float = 0.1                 # 회전 허용 값

    maxX: float = 0.95                    # X축 최대 속도
    maxY: float = 0.2                    # Y축 최대 속도
    maxTH: float = 0.45                   # 회전 최대 속도

    minX: float = 0.05                   # X축 최저 속도
    minY: float = 0.04                   # Y축 최저 속도
    minTH: float = 0.01                  # 회전 최소 속도

    reverse_x_vel: bool = False         # X축 속도 반전
    reverse_y_vel: bool = True         # y축 속도 반전
    reverse_angle_vel: bool = False     # 회전 속도 반전

    x_speed_scale: float = 0.3            # X축 속도 스케일
    y_speed_scale: float = 0.1            # Y축 속도 스케일
    theta_speed_scale: float = 0.004       # 회전 최소 속도

    marginConv: float = 0.0               # 컨베이어 허용 값
    maxConv: float = 0.6                  # 컨베이어 최대 속도
    minConv: float = 0.1                  # 컨베이어 최저 속도
    conv_speed_scale = float = 1.0        # 컨베이어 속도 스케일

    conv_arrival_flag: bool = False     # 컨베이어 도착 감지 여부
    conv_detect_flag: bool = False      # 컨베이어 제품 감지 여부
    conv_input_flag: bool = False       # 컨베이어 투입 감지 여부

    # ====================================================
    #                  ROS Publish 정의
    # ====================================================
    wheel_velocity_pub: rospy.Publisher   # 바퀴 속도 지정 Publish
    conv_velocity_pub: rospy.Publisher    # 컨베이어 속도 지정 Publish
    precise_pub: rospy.Publisher          # 정밀 모드 Publish
    move_state_pub: rospy.Publisher       # 오토모드 상태 Publish

    def __init__(self):
        rospy.init_node('Navigation_Control')

        # 변수 설정
        rate = rospy.Rate(10)               # 10hz

        # Parameter 가져오기
        self.marginX = float(rospy.get_param("~marginX", self.marginX))       # X축 허용 값
        self.marginY = float(rospy.get_param("~marginY", self.marginY))       # Y축 허용 값
        self.marginTH = float(rospy.get_param("~marginTH", self.marginTH))    # 회전 허용 값
        self.maxX = float(rospy.get_param("~maxX", self.maxX))                # X축 최대 속도
        self.maxY = float(rospy.get_param("~maxY", self.maxY))                # Y축 최대 속도
        self.maxTH = float(rospy.get_param("~maxTH", self.maxTH))             # 회전 최대 속도
        self.minX = float(rospy.get_param("~minX", self.minX))                # X축 최저 속도
        self.minY = float(rospy.get_param("~minY", self.minY))                # Y축 최저 속도
        self.minTH = float(rospy.get_param("~minTH", self.minTH))             # 회전 최소 속도
        self.reverse_x_vel = bool(rospy.get_param("~reverse_x_vel", self.reverse_x_vel))                # X축 속도 반전
        self.reverse_y_vel = bool(rospy.get_param("~reverse_y_vel", self.reverse_y_vel))                # y축 속도 반전
        self.reverse_angle_vel = bool(rospy.get_param("~reverse_angle_vel", self.reverse_angle_vel))    # 회전 속도 반전
        self.x_speed_scale = float(rospy.get_param("~x_speed_scale", self.x_speed_scale))               # X축 속도 스케일
        self.y_speed_scale = float(rospy.get_param("~y_speed_scale", self.y_speed_scale))               # Y축 속도 스케일
        self.theta_speed_scale = float(rospy.get_param("~theta_speed_scale", self.theta_speed_scale))   # 회전 최소 속도
        self.marginConv = float(rospy.get_param("~marginConv", self.marginConv))                        # 컨베이어 최대 속도
        self.maxConv = float(rospy.get_param("~maxConv", self.maxConv))                                 # 컨베이어 최대 속도
        self.minConv = float(rospy.get_param("~minConv", self.minConv))                                 # 컨베이어 최저 속도
        self.conv_speed_scale = float(rospy.get_param("~conv_speed_scale", self.conv_speed_scale))      # 컨베이어 속도 스케일

        # Publish 설정
        self.wheel_velocity_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)       # 바퀴 속도 지정 Publish
        self.conv_velocity_pub = rospy.Publisher('/CONV/CMD', convJoy, queue_size=1)     # 컨베이어 속도 지정 Publish
        self.precise_pub = rospy.Publisher('/MOVE/PRECISE', Bool, queue_size=1)         # 정밀모드 Publish
        self.move_state_pub = rospy.Publisher('/MOVE/STATE', cmdMove, queue_size=100)     # 오토모드 상태 Publish

        # Subscriber 설정
        rospy.Subscriber("/MOVE/CMD", cmdMove, self.moveCmdCallback)               # 목표 설정
        rospy.Subscriber("/cmd_vel_joy", teleop, self.joystickControlCallback)   # 조이스틱
        rospy.Subscriber("/agv_pose", Pose2D, self.setCurrentPose)               # 현재 위치 설정
        rospy.Subscriber("/CONV/LIM", limConv, self.convLimCallback)    # 컨베이어 리미트 센서 상태 가져오기

        while not rospy.is_shutdown():
            self.update()
            rate.sleep()

    # 반복 작업
    def update(self):
        if self.manual_mode:
            # 메뉴얼 모드
            print("Manual Mode")
            self.autoModeReset()
            self.sendStateMove(False)
            self.number_data = 0
        else:
            # 오토모드
            print("Auto Mode")
            if self.auto_move == AgvMode.START:
                # 오토 모드 동작 중이라면
                print("Auto Move Start ...")
                self.navControl()
            else:
                # 오토 모드가 동작 중이지 않으면 초기화
                self.autoModeReset()
                self.sendStateMove(False)
                
        self.wheel_velocity_pub.publish(self.velocity)         # 속도 지정 (움직이기)
        self.conv_velocity_pub.publish(self.conv_velocity)     # 컨베이어 속도 지정 (움직이기)
        self.precise_pub.publish(self.precise_mode)            # 정밀 모드 보내기 (움직이기)

    # 자동 이동 컨트롤
    def navControl(self):
        self.sendStateMove(True)

        self.setConvVelocity()
        self.setAGVVelocity()

        if self.move_mode == MoveType.STOP:
            self.move_mode = MoveType.GOAL_REACH

        if self.move_mode == MoveType.XY_MOVE:
            # XY 이동 작업
            self.xyControl(self.target_pose.x, self.target_pose.y)
        
        if self.move_mode == MoveType.ROTATE:
            # 회전 작업
            self.yawControl(self.target_pose.theta, flag=True)

        if self.move_mode == MoveType.COV_MOVE:
            # 컨베이어 작업
            self.convControl(self.target_pose.x)

        if self.move_mode == MoveType.GOAL_REACH:
            # 목표 지점 도달 시
            self.sendStateMove(False)
            self.autoModeReset()  # 초기화

    # 목표 설정
    # def setCMDGoal(self):
    #     goalData: cmdMove = self.command_queue.pop(0)
    #     print("set Goal... : ", goalData)
    #     self.move_mode = MoveType(goalData.type)    # 오토 모드 종류
    #     self.number_data = goalData.number          # 오토 모드 번호
    #     self.target_pose.x = goalData.X             # 타겟 X 축
    #     self.target_pose.y = goalData.Y             # 타겟 Y 축
    #     self.target_pose.theta = goalData.A         # 타겟 방향
    #     self.precise_mode.data = goalData.precise   # 정밀 모드
        
    #     if goalData.move == 1:
    #         # 동작 실행 일 경우
    #         if self.agv_mode == AgvMode.STOP or self.agv_mode == AgvMode.ERROR:
    #             # AGV가 멈춰있는 상태라면 AGV 시작
    #             self.agv_mode = AgvMode.START
    #     else:
    #         if self.agv_mode == AgvMode.START:
    #             # AGV가 동작하고 있는 경우 정지
    #             self.agv_mode = AgvMode.STOP
        
    #     # 속도 초기화
    #     self.setAGVVelocity()
    #     self.setConvVelocity()

    # 오토 모드 초기화
    def autoModeReset(self):
        self.auto_move = AgvMode.STOP
        self.move_mode = MoveType.STOP
        self.target_pose.x = 0.0
        self.target_pose.y = 0.0
        self.target_pose.theta = 0.0
        self.precise_mode.data = False   # 정밀 모드
        self.setAGVVelocity()
        self.setConvVelocity()

    # 자동 움직임 상태 보내기 (True: 시작, False: 완료)
    def sendStateMove(self, flag: bool=False):
        cmd_data: cmdMove = cmdMove()
        cmd_data.move = 1 if flag is True else 0
        cmd_data.type = self.move_mode.value
        cmd_data.number = self.number_data
        cmd_data.X = self.target_pose.x
        cmd_data.Y = self.target_pose.y
        cmd_data.A = self.target_pose.theta
        self.move_state_pub.publish(cmd_data)

    # ====================================================
    #            속도 지정 함수 정의 부분
    # ====================================================
    # XY 이동 작업
    def xyControl(self, goalX: float, goalY: float):
        currentX: float = self.current_pose.x
        currentY: float = self.current_pose.y
        currentA: float = self.current_pose.theta

        delX: float = goalX - currentX
        delY: float = goalY - currentY
        self.lenX = abs(delX)
        self.lenY = abs(delY)

        vel_x: float = 0.0
        vel_y: float = 0.0

        print("delX = ", delX, "(", self.lenX, ")")
        print("delY = ", delY, "(", self.lenY, ")")
        
        self.velocity.angular.z = 0

        print("set XY ... ")
        if self.lenX <= self.marginX and self.lenY <= self.marginY:
            # 목표 좌표로 도착했으면 회전 설정
            self.move_mode = MoveType.ROTATE
        else:
            # X, Y 속도 설정
            if 0 <= currentA <= 5 or 355 <= currentA <= 360:
                # 현재 각도 : 0도
                if 0 <= currentA <= 1 or 359 <= currentA <= 360:
                    max_x_speed = self.maxX
                    max_y_speed = self.maxY
                    self.yawControl(0, True)
                else:
                    max_x_speed = self.maxX
                    max_y_speed = self.maxY
                    # max_x_speed = self.minX
                    # max_y_speed = self.minY
                    self.yawControl(0, True)

                if self.lenX <= self.marginX:
                    vel_x = 0
                else:
                    vel_x = -lrf_tracking_ctrl(self.x_speed_scale, delX, max_x_speed, self.minX)

                if self.lenY <= self.marginY:
                    vel_y = 0
                else:
                    vel_y = lrf_tracking_ctrl(self.y_speed_scale, delY, max_y_speed, self.minY)
            elif 85 <= currentA <= 95:
                # 현재 각도 : 90도
                if 89 <= currentA <= 91:
                    max_x_speed = self.maxX
                    max_y_speed = self.maxY
                    self.yawControl(90, True)
                else:
                    max_x_speed = self.maxX
                    max_y_speed = self.maxY
                    # max_x_speed = self.minX
                    # max_y_speed = self.minY
                    self.yawControl(90, True)

                if self.lenY <= self.marginY:
                    vel_x = 0
                else:
                    vel_x = -lrf_tracking_ctrl(self.x_speed_scale, delY, max_x_speed, self.minX)

                if self.lenX <= self.marginX:
                    vel_y = 0
                else:
                    vel_y = -lrf_tracking_ctrl(self.y_speed_scale, delX, max_y_speed, self.minY)

            elif 175 <= currentA <= 185:
                # 현재 각도 : 180도
                if 179 <= currentA <= 181:
                    max_x_speed = self.maxX
                    max_y_speed = self.maxY
                    self.yawControl(180, True)
                else:
                    max_x_speed = self.maxX
                    max_y_speed = self.maxY
                    # max_x_speed = self.minX
                    # max_y_speed = self.minY
                    self.yawControl(180, True)

                if self.lenX <= self.marginX:
                    vel_x = 0
                else:
                    vel_x = lrf_tracking_ctrl(self.x_speed_scale, delX, max_x_speed, self.minX)

                if self.lenY <= self.marginY:
                    vel_y = 0
                else:
                    vel_y = -lrf_tracking_ctrl(self.y_speed_scale, delY, max_y_speed, self.minY)
                    
            elif 265 <= currentA <= 275:
                # 현재 각도 : 270도
                if 269 <= currentA <= 271:
                    max_x_speed = self.maxX
                    max_y_speed = self.maxY
                    self.yawControl(270, True)
                else:
                    max_x_speed = self.maxX
                    max_y_speed = self.maxY
                    # max_x_speed = self.minX
                    # max_y_speed = self.minY
                    self.yawControl(270, True)

                if self.lenY <= self.marginY:
                    vel_x = 0
                else:
                    vel_x = lrf_tracking_ctrl(self.x_speed_scale, delY, max_x_speed, self.minX)

                if self.lenX <= self.marginX:
                    vel_y = 0
                else:
                    vel_y = lrf_tracking_ctrl(self.y_speed_scale, delX, max_y_speed, self.minY)
            else:
                # 각도가 맞지 않을 경우 각도 맞추기
                if 315 <= currentA <= 360 or 0 <= currentA <= 45:
                    vel_a = 0
                elif 225 <= currentA < 315:
                    vel_a = 270
                elif 135 <= currentA < 225:
                    vel_a = 180
                else:
                    vel_a = 90
                self.yawControl(vel_a)
            
            print("Set Velocity X : ", vel_x)
            print("Set Velocity y : ", vel_y)
            self.velocity.linear.x = vel_x if self.reverse_x_vel is False else vel_x * -1
            self.velocity.linear.y = vel_y if self.reverse_y_vel is False else vel_y * -1
            
    # 회전 컨트롤
    # flag : True일 경우 각도만 조절
    def yawControl(self, goalA: float, xy_fine_tuning=False, flag=False):
        currentA: float = self.current_pose.theta    # 현재 각도
        delTH: float = goalA - currentA    # 남은 각도
        yaw: float = 0.0

        # 회전해야할 각도가 180 이상 넘어 갈 경우
        if delTH >= 180:
            delTH -= 360
        elif delTH <= -180:
            delTH += 360

        self.angTH = abs(delTH)
        print("set YAW ... ")
        if self.angTH <= self.marginTH:
            # 남은 각도가 허용범위 내라면
            print("YAW Control Completion")
            if flag is True:
                # 목표 지점 도달
                if self.lenX <= self.marginX and self.lenY <= self.marginY:
                    self.move_mode = MoveType.GOAL_REACH
                else:
                    self.move_mode = MoveType.XY_MOVE
        else:
            theta_scale: float = self.theta_speed_scale
            # 그렇지 않다면 속도 설정
            if xy_fine_tuning is True and (self.lenX <= 1 or self.lenY <= 1):
                theta_scale = self.theta_speed_scale * 10
            yaw = lrf_tracking_ctrl(theta_scale, delTH, self.maxTH, self.minTH)

            # if angTH <= 10:
            #     yaw = lrf_tracking_ctrl(self.theta_speed_scale, delTH, self.minTH, self.minTH)
            # else:
            #     yaw = lrf_tracking_ctrl(self.theta_speed_scale, delTH, self.maxTH, self.minTH)
            # if 355 <= currentA <= 360 or 0 <= currentA <= 5:
            #     yaw = lrf_tracking_ctrl(self.theta_speed_scale, delTH, self.minTH, self.minTH)
        
        # 속도 설정
        self.velocity.linear.x = 0.0
        self.velocity.linear.y = 0.0
        self.velocity.angular.z = yaw if self.reverse_angle_vel is False else yaw * -1

    # 컨베이어 컨트롤
    def convControl(self, goal: float):
        lim_flag: bool = False   # True가 될 경우 감속 후 정지
        convSpeed: float = 0.0   # 컨베이어 속도

        if goal >= 0:
            # 목표가 양수 (제품출고) (제품 인식이 안되고, 투입 감지가 안될 경우 True)
            lim_flag = True if self.conv_detect_flag is False and self.conv_input_flag is False else False
        else:
            # 목표가 음수 (제품입고) (도착 감지가 될 경우 True)
            lim_flag = self.conv_arrival_flag

        print("set Conv ... ")
        if lim_flag is True:
            # if self.conv_reach_count < 50:
            #     # 컨베이어 도착 카운트가 50 미만일 경우
            #     self.conv_reach_count += 1
            #     convSpeed = lrf_tracking_ctrl(self.conv_speed_scale, self.maxConv*((50-self.conv_reach_count)/50), self.maxConv, self.minConv)
            # else:
            self.move_mode = MoveType.GOAL_REACH
            # self.conv_reach_count = 0
            convSpeed = 0.0
        else:
            # 그렇지 않다면 속도 설정
            convSpeed = lrf_tracking_ctrl(self.conv_speed_scale, self.maxConv*goal, self.maxConv, self.minConv)
        
        # 속도 설정
        self.setConvVelocity(convSpeed)

    # AGV 속도 설정
    def setAGVVelocity(self, x: float=0.0, y: float=0.0, z: float=0.0):
        self.velocity.linear.x = x              # 속도 X 축
        self.velocity.linear.y = y              # 속도 Y 축
        self.velocity.angular.z = z             # 타겟 방향
    
    # 컨베이어 속도 설정 (0일 경우 정지)
    def setConvVelocity(self, speed: float=0):
        self.conv_velocity.mode = 0 if speed == 0 else 1        # 컨베이어 속도 모드
        self.conv_velocity.speed = speed                        # 컨베이어 속도

    # ====================================================
    #                  콜백 함수 정의 부분
    # ====================================================
    # 목표 설정 콜백 함수
    def moveCmdCallback(self, goalData: cmdMove):
        print("receive data: ", goalData)
        self.move_mode = MoveType(goalData.type)    # 오토 모드 종류
        self.number_data = goalData.number          # 오토 모드 번호
        self.target_pose.x = goalData.X             # 타겟 X 축
        self.target_pose.y = goalData.Y             # 타겟 Y 축
        self.target_pose.theta = goalData.A         # 타겟 방향
        self.precise_mode.data = goalData.precise   # 정밀 모드
        
        if goalData.move == 1:
            # 동작 실행 일 경우
            if self.auto_move == AgvMode.STOP or self.auto_move == AgvMode.ERROR:
                # AGV가 멈춰있는 상태라면 AGV 시작
                self.auto_move = AgvMode.START
        else:
            if self.auto_move == AgvMode.START:
                # AGV가 동작하고 있는 경우 정지
                self.auto_move = AgvMode.STOP
        
        # 속도 초기화
        self.setAGVVelocity()
        self.setConvVelocity()


    # 조이스틱 콜백 함수 (메뉴얼 모드인지 파악)
    def joystickControlCallback(self, joystickData: teleop):
        if joystickData.control_mode == 1:
            self.manual_mode = True
        else:
            self.manual_mode = False

    # 현재 위치 설정
    def setCurrentPose(self, poseData):
        self.current_pose = poseData

    # 컨베이어 리미트 센서 상태 가져오기
    def convLimCallback(self, conv_lim_data: limConv):
        self.conv_input_flag = True if conv_lim_data.in_ != 0 else False             # 제품 투입 감지 여부 가져오기
        self.conv_arrival_flag = True if conv_lim_data.arrival != 0 else False      # 제품 도착 감지 여부 가져오기
        self.conv_detect_flag = True if conv_lim_data.mid != 0 else False           # 제품 감지 여부 가져오기


if __name__ == '__main__':
    NavigationControl()