﻿#include "navigation_control.h"

namespace agv_wp
{

NavigationManagement::NavigationManagement()
{

  init();
  param_set();

  goal_pub = nh.advertise<geometry_msgs::Pose2D>("/current_goal", 1, true);
  waypoint_pub = nh.advertise<msgs_node::waypoint>("/MOVE/STATE", 1, true);
  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1, true);
  emc_pub = nh.advertise<std_msgs::UInt8>("/emc_sig", 1, true);
  pub_CHG = nh.advertise<std_msgs::UInt8>("/CHG", 1, true);

  pub_FY = nh.advertise<move_control::convCmd>("/Y/CMD", 1, this);
  pub_FW = nh.advertise<move_control::convCmd>("/W/CMD", 1, this);
  pub_FZ = nh.advertise<move_control::convCmd>("/Z/CMD", 1, this);

  pub_Fstate = nh.advertise<move_control::conv_state>("/conv/STATUS", 1, true);

  goal_sub = nh.subscribe("/MOVE/CMD", 10,
                          &NavigationManagement::goal_target_cb, this);
  joy_sub = nh.subscribe("cmd_vel_joy", 1,
                         &NavigationManagement::joystickControlCallback, this);

  obstacle_sub = nh.subscribe("/obstacle_info", 1,
                              &NavigationManagement::obstacle_cb, this);

  sub_init = nh.subscribe("/init_agv", 1, &NavigationManagement::init_cb, this);
  pub_initPos = nh.advertise<geometry_msgs::PoseWithCovarianceStamped>(
      "/initialpose", 1, true);
  sub_conv = nh.subscribe("/conv/CMD", 10,
                          &NavigationManagement::convActCallback, this);

  sub_Ystatus = nh.subscribe("/Y/STATUS", 1,
                             &NavigationManagement::YstatusCallback, this);
  sub_Wstatus = nh.subscribe("/W/STATUS", 1,
                             &NavigationManagement::WstatusCallback, this);
  sub_Zstatus = nh.subscribe("/Z/STATUS", 1,
                             &NavigationManagement::ZstatusCallback, this);
  sub_motor_status = nh.subscribe("/left_front/status", 1,
                                  &NavigationManagement::motor_status_cb, this);
}

NavigationManagement::~NavigationManagement() {}

void NavigationManagement::init()
{
  rate = 10; // 10; //100
  target_pos_x = 0.0;
  target_pos_y = 0.0;
  target_A = 0.0;

  goal_A = 0.0;
  goal_x = 0.0;
  goal_y = 0.0;

  current_x = 0;
  current_y = 0;
  current_A = 0;

  cur_x = 0;
  cur_y = 0;
  cur_A = 0;

  agvPos_ = H_AGV;
  // agvPos_ = H_AGV2;

  agvGoal_ = NO_WHERE;
  agvSubGoal_ = NO_WHERE;
  move_point_ = NO_WHERE;
  mode_agv_ = mode_stop_;
  wp_ = NO_POINT_;
  moveMode_ = MOVE_ZERO_;

  obstacle_ = false;
  manual_mode = false;
  move_state = 0;

  count = 0;

  cntF_ = 0;
  mode_conv_ = conv_stop_;
  conv_task_ = NO_conv;
  convStatus_ = conv_STP;
}

// 물체 감지
// 물체 감지 개수가 1개 이상일 경우 mode_obs(물체 감지 상태) 설정
void NavigationManagement::obstacle_cb(
    const obstacle_detector::obstacles_detect msg)
{
  if (msg.number == 0)
  {
    if (mode_agv_ == mode_obs_)
      mode_agv_ = mode_start_;
  }
  else
  {
    if (mode_agv_ == mode_start_)
      mode_agv_ = mode_obs_;
  }
}

void NavigationManagement::motor_status_cb(motor_driver_msgs::status msg)
{
  _motor_status = msg.status;
  if(msg.bat_vol>595)
  {
      std_msgs::UInt8 chg_msgs;
      chg_msgs.data = 0;
      pub_CHG.publish(chg_msgs);
  }

}

void NavigationManagement::joystickControlCallback(
    const move_control::teleop::ConstPtr& motorCmd_)
{
  if (motorCmd_->control_mode == 1 || motorCmd_->control_mode == 2)
  {
    manual_mode = true;
  }
  else
    manual_mode = false;
}

void NavigationManagement::YstatusCallback(move_control::convStatus msg)
{
  yState.Dist = msg.x;
  yState.Pos = msg.pos;
  yState.Mode = msg.mode;
  yState.Status = msg.status;
}

void NavigationManagement::ZstatusCallback(move_control::convStatus msg)
{
  zState.Dist = msg.x;
  zState.Pos = msg.pos;
  zState.Mode = msg.mode;
  zState.Status = msg.status;
}

void NavigationManagement::WstatusCallback(move_control::convStatus msg)
{
  wState.Dist = msg.x;
  wState.Pos = msg.pos;
  wState.Mode = msg.mode;
  wState.Status = msg.status;
}

void NavigationManagement::convActCallback(
    const move_control::conv_act::ConstPtr& convCmd_)
{
  // TAKE IN(1), TAKE OUT(2), STOP(0)

  if (convCmd_->run == 1)
  {
    target_hgt = (double)convCmd_->hgt;
    target_len = (double)convCmd_->len;
    target_wid = (double)convCmd_->wid;

    if (mode_conv_ == conv_stop_)
    {
      switch (convCmd_->mode)
      {
      case T_IN: // TAKE IN
      {
        mode_conv_ = conv_in;
        pF_ = 1;
        pFs_ = sizeof(TAKE_IN) / sizeof(TAKE_IN[0]);
      }
      break;
      case T_OUT: // TAKE OUT
      {

        mode_conv_ = conv_out;
        pF_ = 1;
        pFs_ = sizeof(TAKE_OUT) / sizeof(TAKE_OUT[0]);
      }
      break;
      case P_GND: // PUT GND
      {

        mode_conv_ = conv_gnd;
        pF_ = 1;
        pFs_ = sizeof(PUT_GND) / sizeof(PUT_GND[0]);
      }
      break;
      case L_GND: // LIFT FROM GND
      {

        mode_conv_ = conv_lift_gnd;
        pF_ = 1;
        pFs_ = sizeof(LIFT_GND) / sizeof(LIFT_GND[0]);
      }
      break;
      default:
        break;
      }
    }
  }
  else
  {
    mode_conv_ = conv_stop_;
  }
}

void NavigationManagement::init_cb(const std_msgs::UInt8::ConstPtr& msg)
{
  switch (msg->data)
  {
  case 1:
  {
    agvPos_ = H_AGV;
    agvStatus_ = GOAL_REACH;
    geometry_msgs::PoseWithCovarianceStamped init_;
    init_.header.stamp = ros::Time::now();
    init_.header.frame_id = "map";

    /*	init_.pose.pose.position.x = INIT_POS_X;
      init_.pose.pose.position.y = INIT_POS_Y;*/
    init_.pose.pose.position.x = H1_X;
    init_.pose.pose.position.y = H1_Y;
    init_.pose.pose.position.z = 0.0;

    init_.pose.pose.orientation = tf::createQuaternionMsgFromYaw(
        static_cast<double>(H1_A * M_PI / 180.0));

    pub_initPos.publish(init_);

    cur_goal_.x = H1_X;
    cur_goal_.y = H1_Y;
    cur_goal_.theta = H1_A;
    goal_pub.publish(cur_goal_);
  }
  break;
  }
}

void NavigationManagement::wp_pub()
{
  msgs_node::waypoint wp_msgs;
  wp_msgs.goal = agvGoal_;
  wp_msgs.sub_goal = agvSubGoal_;
  // wp_msgs.pos = agvPos_;
  wp_msgs.pos = agvPos_;
  wp_msgs.status = agvStatus_;
  wp_msgs.move_pt = move_point_;

  switch (wp_)
  {
  case NO_POINT_:
    wp_msgs.wp = 0;
    break;
  case INDIRECT_:
    wp_msgs.wp = 1;
    break;
  case DIRECT_:
    wp_msgs.wp = 2;
    break;
  }
  switch (lrfMove_)
  {
  case LRF_XY_:
    wp_msgs.mode = 1;
    break;
  case LRF_DIAG:
    wp_msgs.mode = 2;
    break;
  case LRF_YAW_:
    wp_msgs.mode = 3;
    break;
  case LRF_NONE_:
    wp_msgs.mode = 0;
    break;
  default:
    wp_msgs.mode = 0;
    break;
  }
  wp_msgs.X = current_x;
  wp_msgs.Y = current_y;
  wp_msgs.A = current_A;
  waypoint_pub.publish(wp_msgs);
}

void NavigationManagement::update()
{

  geometry_msgs::TransformStamped transformStamped;
  try
  {
    transformStamped = tfBuffer.lookupTransform("map", "base_link", ros::Time(0));
    current_x = -transformStamped.transform.translation.x;
    current_y = -transformStamped.transform.translation.y;

    tf::Quaternion q(transformStamped.transform.rotation.x,
                     transformStamped.transform.rotation.y,
                     transformStamped.transform.rotation.z,
                     transformStamped.transform.rotation.w);
    double roll, pitch, yaw;
    tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
    current_A = yaw * 180 / M_PI + 180;
  }
  catch (tf2::TransformException& ex)
  {
    ROS_WARN("%s", ex.what());
    ros::Duration(0.01).sleep();
    //		continue;
  }
  std_msgs::UInt8 emc_data;

  if (manual_mode)
  {
    agvGoal_ = NO_WHERE;
    agvSubGoal_ = NO_WHERE;
    move_point_ = NO_WHERE;
    mode_agv_ = mode_stop_;
    wp_ = NO_POINT_;
    moveMode_ = MOVE_ZERO_;
    stop_agv();
    lrfMove_ = LRF_NONE_;

    count = 0;
    pt_ = 0;
    pts_ = 0;

    agvStatus_ = MANUAL;
    lrf_fail = false;
    emc_data.data = 0;
    emc_pub.publish(emc_data);
    init_cnt_ = 0;

    mode_conv_ = conv_stop_;
    pF_ = 0;
    pFs_ = 0;
    cntF_ = 0;
    conv_task_ = NO_conv;
    stop_conv();
  }
  else
  {
    stop_agv();
    switch (mode_agv_)
    {
    case mode_obs_:
    {
      // count = 0;
      obs_detect = true;
      stop_agv();
      agvStatus_ = OBS;
      lrf_fail = false;
      init_cnt_ = 0;
      emc_data.data = 1;
      emc_pub.publish(emc_data);
    }
    break;
    case mode_emc_:
    {
      count = 0;
      stop_agv();
      emc_data.data = 2;
      emc_pub.publish(emc_data);
      init_cnt_ = 0;
    }
    break;
        /*
    case mode_motor_er:
    {
        count = 0;
        cnt_mot_er = 0;
        stop_agv();
        // Kill node
        QStringList list;
        list << "kill"
             << "front_right"
             << "front_left"
             << "rear_right"
             << "rear_left"
             << "conv_driver_z"
             << "conv_driver_yw";
        process.execute("rosnode", list);
        ros::Duration(0.1).sleep();
        // Restore
        process.startDetached("roslaunch wheel_driver wheel_driver.launch");
        ros::Duration(0.1).sleep();
        process.startDetached("roslaunch conv_driver conv_driver.launch");

        if(_motor_status == 1)
        {
            ros::Duration(0.1).sleep();
            mode_agv_ = mode_start_;
        }
    }
    break;
    */

    case mode_start_:
    {
      emc_data.data = 0;
      emc_pub.publish(emc_data);
      /*
      if(_motor_status == 0) cnt_mot_er++;
      else cnt_mot_er = 0;


      if (_motor_status == 1 || (_motor_status == 0 && cnt_mot_er<20))
      {

      }
      else
      {
          mode_agv_ = mode_motor_er;
          if(mode_conv_ != conv_emc || mode_conv_ !=conv_stop_)
          {
              mode_conv_ = conv_emc;
          }
      }
      */

      switch (wp_)
    {
    case INDIRECT_:
    {
      if (pt_ > 0 && pt_ <= pts_)
        move_point_ = PATH[pt_ - 1];
      else
      {
        move_point_ = NO_WHERE;
        mode_agv_ = mode_stop_;
        wp_ = NO_POINT_;
        moveMode_ = MOVE_ZERO_;
      }
    }
    break;

    case DIRECT_:
    {
      if (pt_ > 0 && pt_ <= pts_)
        move_point_ = GOAL;
      else
      {
        move_point_ = NO_WHERE;
        mode_agv_ = mode_stop_;
        wp_ = NO_POINT_;
        moveMode_ = MOVE_ZERO_;
      }
    }
    break;
    }

    switch (move_point_)
    {
    case WP_CUR:
      wp_cur_move();
      break;
    case WP_GOAL:
      wp_goal_move();
      break;
    case GOAL:
      goal_move();
      break;
    case WP_MID2:
      WP_MID2_move();
      break;
    case WP_MID1:
      WP_MID1_move();
      break;
    case WP_MID3:
      WP_MID3_move();
      break;
    default:
      break;
    }
    }
    break;
    case mode_stop_:
    {
      count = 0;
      init_cnt_ = 0;
      stop_agv();
      lrfMove_ = LRF_NONE_;
      if (agvStatus_ != GOAL_REACH)
        agvStatus_ = AGV_STP;
    }
    break;
    }

    switch (mode_conv_)
    {
    case conv_stop_:
    {
      // if (convStatus_ != IN_FSH || convStatus_ != OUT_FSH)
      //  convStatus_ = conv_STP;
      stop_conv();
      conv_mode_ = 0;
      if (convStatus_ != F_GOAL)
        convStatus_ = conv_STP;
    }
    break;
    case conv_in:
    {
        /*
        if(_motor_status == 0) cnt_mot_er++;
        else cnt_mot_er = 0;

        if(_motor_status == 1 || (_motor_status == 0 && cnt_mot_er<20))
        {

        }
        else
        {
            conv_pre_ = mode_conv_;
            mode_conv_ = conv_emc;
            if(mode_agv_ == mode_start_)
            {
                mode_agv_ = mode_motor_er;
            }
        }
        */

        if (pF_ > 0 && pF_ <= pFs_)
            conv_task_ = TAKE_IN[pF_ - 1];
        else
        {
            conv_task_ = NO_conv;
            mode_conv_ = conv_stop_;
            conv_mode_ = 0;
        }

        switch (conv_task_)
        {
        case NO_conv:
        {
            stop_conv();
            conv_mode_ = 0;
        }
            break;
        case Z_H:
        {
            z_h();
            conv_mode_ = Z_H;
        }
            break;
        case Z_DOWN:
        {
            z_down();
            conv_mode_ = Z_DOWN;
        }
            break;
        case Y_FORW:
        {
            y_forw();
            conv_mode_ = Y_FORW;
        }
            break;
        case Y_BACKW:
        {
            y_backw();
            conv_mode_ = Y_BACKW;
        }
            break;
        case W_OUT:
        {
            w_out();
            conv_mode_ = Y_BACKW;
        }
            break;
        default:
            break;
        }
    }
    break;

    case conv_out:
    {
        if(_motor_status == 0) cnt_mot_er++;
        else cnt_mot_er = 0;
        if(_motor_status == 1 || (_motor_status == 0 && cnt_mot_er<20))
        {

        }
        else
        {
            conv_pre_ = mode_conv_;
            mode_conv_ = conv_emc;
            if(mode_agv_ == mode_start_)
            {
                mode_agv_ = mode_motor_er;
            }
        }
    }
    break;

    case conv_gnd:
    {
        /*
        if(_motor_status == 0) cnt_mot_er++;
        else cnt_mot_er = 0;
        if(_motor_status == 1 || (_motor_status == 0 && cnt_mot_er<20))
        {
            if (pF_ > 0 && pF_ <= pFs_)
                conv_task_ = PUT_GND[pF_ - 1];
            else
            {
                conv_task_ = NO_conv;
                mode_conv_ = conv_stop_;
                conv_mode_ = 0;
            }

            switch (conv_task_)
            {
            case NO_conv:
            {
                stop_conv();
                conv_mode_ = 0;
            }
                break;
            case Z_DOWN:
            {
                z_down();
                conv_mode_ = Z_DOWN;
            }
                break;
            case Y_FORW:
            {
                y_forw();
                conv_mode_ = Y_FORW;
            }
                break;
            default:
                break;
            }
        }
        else
        {
            conv_pre_ = mode_conv_;
            mode_conv_ = conv_emc;
            if(mode_agv_ == mode_start_)
            {
                mode_agv_ = mode_motor_er;
            }
        }
        */

        if (pF_ > 0 && pF_ <= pFs_)
            conv_task_ = TAKE_OUT[pF_ - 1];
        else
        {
            conv_task_ = NO_conv;
            mode_conv_ = conv_stop_;
            conv_mode_ = 0;
        }

        switch (conv_task_)
        {
        case NO_conv:
        {
            stop_conv();
            conv_mode_ = 0;
        }
            break;
        case Z_H:
        {
            z_h();
            conv_mode_ = Z_H;
        }
            break;
        case Z_UP:
        {
            z_up();
            conv_mode_ = Z_UP;
        }
            break;
        case Z_DOWN:
        {
            z_down();
            conv_mode_ = Z_DOWN;
        }
            break;
        case Y_FORW:
        {
            y_forw();
            conv_mode_ = Y_FORW;
        }
            break;
        case Y_BACKW:
        {
            y_backw();
            conv_mode_ = Y_BACKW;
        }
            break;
        case W_OUT:
        {
            w_out();
            conv_mode_ = Y_BACKW;
        }
            break;
        default:
            break;
        }
    }
    break;

    case conv_lift_gnd:
    {
        /*
        if(_motor_status == 0) cnt_mot_er++;
        else cnt_mot_er = 0;
        if(_motor_status == 1 || (_motor_status == 0 && cnt_mot_er<20))
        {

        }
        else
        {
            conv_pre_ = mode_conv_;
            mode_conv_ = conv_emc;
            if(mode_agv_ == mode_start_)
            {
                mode_agv_ = mode_motor_er;
            }
        }
        */
        if (pF_ > 0 && pF_ <= pFs_)
            conv_task_ = LIFT_GND[pF_ - 1];
        else
        {
            conv_task_ = NO_conv;
            mode_conv_ = conv_stop_;
            conv_mode_ = 0;
        }

        switch (conv_task_)
        {
        case NO_conv:
        {
            stop_conv();
            conv_mode_ = 0;
        }
            break;
        case Z_H:
        {
            z_h();
            conv_mode_ = Z_H;
        }
            break;
        case Y_BACKW:
        {
            y_backw();
            conv_mode_ = Y_BACKW;
        }
            break;
        default:
            break;
        }
    }
    break;

    case conv_emc:
    {
      stop_conv();
      cnt_mot_er = 0;
      conv_task_ = NO_conv;
      conv_mode_ = 0;
      convStatus_ = conv_STP;
      /*
      // Kill node
      QStringList list;
      list << "kill"
           << "right_wheel"
           << "left_wheel"
           << "conv_driver_z"
           << "conv_driver_yw";
      process.execute("rosnode", list);
      ros::Duration(0.1).sleep();
      // Restore
      process.startDetached("roslaunch wheel_driver wheel_driver.launch");
      ros::Duration(0.1).sleep();
      process.startDetached("roslaunch conv_driver conv_driver.launch");

      if(_motor_status == 1)
      {
          ros::Duration(0.1).sleep();
          mode_conv_ = conv_pre_;
      }
      */

    }
    break;

    case conv_zhome:
    {
      z_h();
      conv_mode_ = Z_H;
    }
    break;
    }
  }
  if (!recover_)
  {
    old_x = current_x;
    old_y = current_y;
    old_A = current_A;
    old_vx = vel_.linear.x;
    old_vy = vel_.linear.y;
    old_vA = vel_.angular.z;
  }
  vel_pub.publish(vel_);
  _motor_status = 0;
  wp_pub();
  conv_pub();
}


void NavigationManagement::spin()
{
  ros::Rate loop_rate(rate);
  tf2_ros::TransformListener tfListener(tfBuffer);

  while (ros::ok())
  {
    update();
    ros::spinOnce();
    loop_rate.sleep();
  }
}

void NavigationManagement::stop_conv()
{
  if (wState.Status == F_MOV)
  {
    move_control::convCmd w_cmd;
    w_cmd.operate = 0;
    w_cmd.goal = 0.0;
    pub_FW.publish(w_cmd);
  }
  if (yState.Status == F_MOV)
  {
    move_control::convCmd y_cmd;
    y_cmd.operate = 0;
    y_cmd.goal = 0.0;
    pub_FY.publish(y_cmd);
  }
  if (zState.Status == F_MOV)
  {
    move_control::convCmd z_cmd;
    z_cmd.operate = 0;
    z_cmd.goal = 0.0;
    pub_FZ.publish(z_cmd);
  }
}

void NavigationManagement::z_h()
{
  switch (zState.Status)
  {
  case F_STP:
  {
    move_control::convCmd z_cmd;
    z_cmd.operate = 1;
    z_cmd.goal = 3.0;
    pub_FZ.publish(z_cmd);
  }
  break;
  case F_MOV:
  {
    convStatus_ = conv_MOV;
  }
  break;
  case F_GOAL:
  {
    if (zState.Pos == F_HOME || fabs(zState.Dist - 0.0) < 5.0)
    {
      // int8_t size_o_ = sizeof(TAKE_OUT) / sizeof(TAKE_OUT[0]);
      int8_t size_i_ = sizeof(TAKE_IN) / sizeof(TAKE_IN[0]);
      if (mode_conv_ == conv_zhome)
      {
        mode_conv_ = conv_stop_;
        convStatus_ = conv_GOAL;
        pF_ = 0;
      }
      else
      {
        if (pF_ == size_i_ && mode_conv_ == conv_in)
        {
          mode_conv_ = conv_stop_;
          convStatus_ = conv_GOAL;
          pF_ = 0;
        }
        else
        {
          pF_++;
        }
      }
    }
    else
    {
      move_control::convCmd z_cmd;
      z_cmd.operate = 1;
      z_cmd.goal = 3.0;
      pub_FZ.publish(z_cmd);
    }
  }
  break;
  default:
    break;
  }
}
void NavigationManagement::z_up()
{
  switch (zState.Status)
  {
  case F_STP:
  {
    move_control::convCmd z_cmd;
    z_cmd.operate = 1;
    z_cmd.goal = z_goal;
    pub_FZ.publish(z_cmd);
  }
  break;
  case F_MOV:
  {
    convStatus_ = conv_MOV;
  }
  break;
  case F_GOAL:
  {
    if (fabs(zState.Dist - z_goal) < 5.0)
      pF_++;
    else
    {
      move_control::convCmd z_cmd;
      z_cmd.operate = 1;
      z_cmd.goal = z_goal;
      pub_FZ.publish(z_cmd);
    }
  }
  break;
  default:
    break;
  }
}
void NavigationManagement::z_down()
{
  switch (zState.Status)
  {
  case F_STP:
  {
    move_control::convCmd z_cmd;
    z_cmd.operate = 1;
    z_cmd.goal = target_hgt;
    pub_FZ.publish(z_cmd);
  }
  break;
  case F_MOV:
  {
    convStatus_ = conv_MOV;
  }
  break;
  case F_GOAL:
  {
    if (fabs(zState.Dist - target_hgt) < 5.0)
    {
      int8_t sizeG_ = sizeof(PUT_GND) / sizeof(PUT_GND[0]);
      if (pF_ == sizeG_ && mode_conv_ == conv_gnd)
      {
        mode_conv_ = conv_stop_;
        convStatus_ = conv_GOAL;
        pF_ = 0;
      }
      else
        pF_++;
    }
    else
    {
      move_control::convCmd z_cmd;
      z_cmd.operate = 1;
      z_cmd.goal = target_hgt;
      pub_FZ.publish(z_cmd);
    }
  }
  break;
  default:
    break;
  }
}

void NavigationManagement::y_backw()
{
  switch (yState.Status)
  {
  case F_STP:
  {
    if (zState.Dist > -600.0)
    {
      move_control::convCmd y_cmd;
      y_cmd.operate = 1;
      y_cmd.goal = -2.0;
      pub_FY.publish(y_cmd);
    }
    else
    {
      mode_conv_ = conv_stop_;
    }
  }
  break;
  case F_MOV:
  {
    convStatus_ = conv_MOV;
  }
  break;
  case F_GOAL:
  {
    if (yState.Pos == F_HOME || (fabs(yState.Dist) < 5.0))
    {

      int8_t sizeO_ = sizeof(TAKE_OUT) / sizeof(TAKE_OUT[0]);
      int8_t sizeL_ = sizeof(LIFT_GND) / sizeof(LIFT_GND[0]);
      if (pF_ == sizeO_ && mode_conv_ == conv_out)
      {
        mode_conv_ = conv_stop_;
        convStatus_ = conv_GOAL;
        pF_ = 0;
      }
      else if (pF_ == sizeL_ && mode_conv_ == conv_lift_gnd)
      {
        mode_conv_ = conv_stop_;
        convStatus_ = conv_GOAL;
        pF_ = 0;
      }
      else
      {
        pF_++;
      }

      // pF_++;
    }
    else
    {
      if (zState.Dist > -600.0)
      {
        move_control::convCmd y_cmd;
        y_cmd.operate = 1;
        y_cmd.goal = -2.0;
        pub_FY.publish(y_cmd);
      }
      else
      {
        mode_conv_ = conv_stop_;
      }
    }
  }
  break;
  default:
    break;
  }
}

void NavigationManagement::pgv_status_cb(const std_msgs::UInt8& msg)
{
  pgvStatus_ = msg.data;
}

void NavigationManagement::pgv_cb(const pgv::vision_msg& pgv_msg)
{
  control_id = pgv_msg.ControlID;
  control_ = pgv_msg.Control;
  tag_ = pgv_msg.Tag;
  tag_id = pgv_msg.TagID;

  line_x = pgv_msg.X;
  line_y = pgv_msg.Y;
  line_theta = pgv_msg.theta;
  fault = pgv_msg.Fault;
  warning = pgv_msg.Warning;
  command = pgv_msg.command;
}

void NavigationManagement::qr_ctrl()
{
  if (tag_ == 1)
  {
    if (fabs(line_x) <= 2 && fabs(line_y) <= 2 && line_theta > 178 &&
        line_theta < 180)
    {
      vel_.linear.x = 0;
      vel_.linear.y = 0;
      vel_.angular.z = 0;
      agvStatus_ = QR_COMP;
    }
    else
    {
      if (fabs(line_x) > 2)
        vel_.linear.x = -QR_VEL * sign(line_x);
      else
        vel_.linear.x = 0;

      if (fabs(line_y) > 2)
        vel_.linear.y = -QR_VEL * sign(line_y);
      else
        vel_.linear.y = 0;

      if (line_theta > 140 && line_theta < 220)
      {
        if (line_theta >= 180 || line_theta <= 178)
          vel_.angular.z = QR_AVEL * sign(line_theta - 179);
        else
          vel_.angular.z = 0;
      }
      else
        vel_.angular.z = 0;
      agvStatus_ = QR_RUN;
    }
  }
  else
  {
    vel_.linear.x = 0;
    vel_.linear.y = 0;
    vel_.angular.z = 0;
    agvStatus_ = AGV_STP;
  }
}

void NavigationManagement::y_forw()
{
  switch (yState.Status)
  {
  case F_STP:
  {
    move_control::convCmd y_cmd;
    y_cmd.operate = 1;
    y_cmd.goal = target_len;
    pub_FY.publish(y_cmd);
  }
  break;
  case F_MOV:
  {
    convStatus_ = conv_MOV;
  }
  break;
  case F_GOAL:
  {
    if (yState.Pos == LIM || (fabs(yState.Dist - target_len) < 5.0))
    {
      pF_++;
      if (zState.Dist <= -305.0)
        z_goal = -200.0;
      else if (zState.Dist > -100)
        z_goal = 0;
      else
        z_goal = target_hgt + 100.0;
    }
    else
    {
      move_control::convCmd y_cmd;
      y_cmd.operate = 1;
      y_cmd.goal = target_len;
      pub_FY.publish(y_cmd);
    }
  }
  break;
  default:
    break;
  }
}

void NavigationManagement::w_out()
{
  switch (wState.Status)
  {
  case F_STP:
  {
    move_control::convCmd w_cmd;
    w_cmd.operate = 1;
    w_cmd.goal = target_wid;
    pub_FW.publish(w_cmd);
  }
  break;
  case F_MOV:
  {
    convStatus_ = conv_MOV;
  }
  break;
  case F_GOAL:
  {
    if (fabs(wState.Dist - target_wid) < 5.0)
    {
      pF_++;
    }
    else
    {
      move_control::convCmd w_cmd;
      w_cmd.operate = 1;
      w_cmd.goal = target_wid;
      pub_FW.publish(w_cmd);
    }
  }
  break;
  default:
    break;
  }
}

void NavigationManagement::conv_pub()
{
  move_control::conv_state f_state;

  f_state.mode = conv_mode_;

  switch (convStatus_)
  {
  case conv_STP:
    f_state.status = 0;
    break;
  case conv_MAN:
    f_state.status = 1;
    break;
  case conv_MOV:
    f_state.status = 2;
    break;
  case conv_GOAL:
    f_state.status = 3;
    break;
  }
  f_state.y = yState.Dist;
  f_state.w = wState.Dist;
  f_state.z = zState.Dist;
  pub_Fstate.publish(f_state);
}

double NavigationManagement::lrf_tracking_ctrl(double Kp, double dx,
                                               double vmax, double vmin)
{
  double v_cal = Kp * dx;
  if (v_cal >= 0)
  {
    if (v_cal > vmax)
      v_cal = vmax;
    else if (v_cal < vmin)
      v_cal = vmin;
  }
  else
  {
    if (v_cal < -vmax)
      v_cal = -vmax;
    else if (v_cal > -vmin)
      v_cal = -vmin;
  }
  return v_cal;
}

void NavigationManagement::XY_control()
{
  delX_ = goal_x - current_x;
  delY_ = goal_y - current_y;
  lenX_ = fabs(goal_x - cur_x);
  lenY_ = fabs(goal_y - cur_y);
  // vel_.angular.z = 0;

  if (fabs(delY_) <= delY_TOL && fabs(delX_) <= delX_TOL)
  {
    vel_.linear.x = 0;
    vel_.linear.y = 0;
    vel_.angular.z = 0;
    agvStatus_ = LRF_XY_COMP;
  }
  else
  {
    agvStatus_ = LRF_RUN;
    // if(cur_A>180) cur_A=cur_A-360.0;

    if ((cur_A >= 0 && cur_A < 10) ||
        (cur_A <= 360 && cur_A > 350)) // Around 0 deg
    {
      if (lenX_ > 2)
        vel_.linear.x = lrf_tracking_ctrl(Kp_, delX_, LVX_MAX, 0.005);
      else
        vel_.linear.x = lrf_tracking_ctrl(Kp_, delX_, LVX_MAX * 0.7, 0.005);

      if (lenY_ > 3)
        vel_.linear.y = lrf_tracking_ctrl(Kp_, delY_, LVY_MAX, 0.005);
      else
        vel_.linear.y = lrf_tracking_ctrl(Kp_, delY_, LVY_MAX * 0.7, 0.005);

      delTH_ = goal_A - current_A;
      if (delTH_ > 180)
        delTH_ = delTH_ - 360;
      else if (delTH_ < -180)
        delTH_ = delTH_ + 360;
      if (fabs(delTH_) > 10 || fabs(delTH_) <= delTH_TOL)
      {
        vel_.angular.z = 0;
      }
      else
      {
        vel_.angular.z = lrf_tracking_ctrl(0.008, delTH_, LVTH_MAX, 0.008);
      }
    }
    else if (cur_A > 80 && cur_A < 100)
    {
      if (lenY_ > 2)
        vel_.linear.x = lrf_tracking_ctrl(Kp_, delY_, LVX_MAX, 0.005);
      else
        vel_.linear.x = lrf_tracking_ctrl(Kp_, delY_, LVX_MAX * 0.7, 0.005);

      if (lenX_ > 3)
        vel_.linear.y = -lrf_tracking_ctrl(Kp_, delX_, LVY_MAX, 0.005);
      else
        vel_.linear.y = -lrf_tracking_ctrl(Kp_, delX_, LVY_MAX * 0.7, 0.005);

      vel_.angular.z = 0;
    }
    else if ((cur_A > 170 && cur_A < 190))
    {
      if (lenY_ > 2)
        vel_.linear.y = -lrf_tracking_ctrl(Kp_, delY_, LVY_MAX, 0.005);
      else
        vel_.linear.y = -lrf_tracking_ctrl(Kp_, delY_, LVY_MAX * 0.7, 0.005);

      if (lenX_ > 3)
        vel_.linear.x = -lrf_tracking_ctrl(Kp_, delX_, LVX_MAX, 0.005);
      else
        vel_.linear.x = -lrf_tracking_ctrl(Kp_, delX_, LVX_MAX * 0.7, 0.005);

      delTH_ = goal_A - current_A;
      if (delTH_ > 180)
        delTH_ = delTH_ - 360;
      else if (delTH_ < -180)
        delTH_ = delTH_ + 360;
      if (fabs(delTH_) > 10 || fabs(delTH_) <= delTH_TOL)
      {
        vel_.angular.z = 0;
      }
      else
      {
        vel_.angular.z = lrf_tracking_ctrl(0.008, delTH_, LVTH_MAX, 0.008);
      }
    }
    else if (cur_A >= 260 && cur_A <= 280) // around -90
    {
      if (lenY_ > 2)
        vel_.linear.x = -lrf_tracking_ctrl(Kp_, delY_, LVX_MAX, 0.005);
      else
        vel_.linear.x = -lrf_tracking_ctrl(Kp_, delY_, LVX_MAX * 0.7, 0.005);
      if (lenX_ > 3)
        vel_.linear.y = lrf_tracking_ctrl(Kp_, delX_, LVY_MAX, 0.005);
      else
        vel_.linear.y = lrf_tracking_ctrl(Kp_, delX_, LVY_MAX * 0.7, 0.005);

      vel_.angular.z = 0;
    }
  }
  // vel_pub.publish(vel_);
}

void NavigationManagement::YAW_control()
{
  delTH_ = goal_A - current_A;
  vel_.linear.x = 0;
  vel_.linear.y = 0;
  if (delTH_ > 180)
    delTH_ = delTH_ - 360;
  else if (delTH_ < -180)
    delTH_ = delTH_ + 360;
  if (fabs(delTH_) <= delTH_TOL)
  {
    vel_.angular.z = 0;
    agvStatus_ = WP_REACH;
    lrfMove_ = LRF_NONE_;
  }
  else
  {
    vel_.angular.z = lrf_tracking_ctrl(0.008, delTH_, LVTH_MAX, 0.008);
  }
  // vel_pub.publish(vel_);
}

void NavigationManagement::nav_control()
{
  switch (moveMode_)
  {
  case MOVE_LRF_:
  {
    switch (lrfMove_)
    {
    case LRF_XY_:
      XY_control();
      break;
    case LRF_YAW_:
      YAW_control();
      break;
    case LRF_DIAG:
      diag_control();
    }
  }
  break;

  case MOVE_ZERO_:
  {
    stop_agv();
    if (agvStatus_ != GOAL_REACH)
      agvStatus_ = AGV_STP;
  }
  break;
  case MOVE_QR_:
  {
    qr_ctrl();
  }
  break;
  }
}

void NavigationManagement::goal_target_cb(const msgs_node::cmdMove& goal)
{
  ROS_INFO("Goal received ... ");

  target_pos_x = goal.X / 1000.0;
  target_pos_y = goal.Y / 1000.0;
  target_A = goal.A / 1000.0;

  agvGoal_ = goal.GL;

  count = 0;

  if (goal.move == 1)
  {
    if (mode_agv_ == mode_stop_ || mode_agv_ == mode_emc_)
      mode_agv_ = mode_start_;
  }
  else
  {
    if (mode_agv_ == mode_start_)
      mode_agv_ = mode_stop_;
  }
  if (agvPos_ == GND && agvGoal_ == BACK_GND)
  {
    wp_ = DIRECT_;
    pt_ = 1;
    pts_ = 1;
  }
  else
  {
    wp_ = INDIRECT_;
    pt_ = 1;
    pts_ = sizeof(PATH) / sizeof(PATH[0]);
  }
}

float NavigationManagement::sign(float in)
{
  if (in > 0)
    return 1;
  else if (in < 0)
    return -1;
  else
    return 0;
}

void NavigationManagement::wp_cur_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (agvPos_ == CHG)
    {
      std_msgs::UInt8 chg_data;
      chg_data.data = 0;
      pub_CHG.publish(chg_data);
    }

    if (current_x >= RB1_WP[0] + 1.0)
    {
      goal_x = cur_x;
      goal_y = RB1_WP[1];
      goal_A = cur_A;
    }
    else
    {
      if (current_x < MID_WP[0] + 0.5)
      {
        goal_x = cur_x;
        goal_y = MID_WP[1];
        goal_A = cur_A;
      }
      else
      {
        if (cur_A > 80 && cur_A < 100)
        {
          goal_x = cur_x;
          goal_y = MID_WP[1];
          goal_A = cur_A;
        }
        else
        {
          goal_x = MID_WP[0];
          goal_y = cur_y;
          goal_A = cur_A;
        }
      }
    }
    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();
  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    count = 0;
    pt_++;
  }
}

void NavigationManagement::WP_MID1_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;

    if (current_y >= (RB1_WP[1] - 0.5))
    {
      if (agvGoal_ == RB1)
      {
        goal_x = cur_x;
        goal_y = RB1_WP[1];
        goal_A = cur_A;
      }
      else
      {
        goal_x = RB1_WP[0];
        goal_y = RB1_WP[1];
        goal_A = RB1_WP[2];
      }
    }
    else
    {
      if (agvGoal_ == RB1)
      {
        goal_x = MID_WP[0];
        goal_y = MID_WP[1];
        goal_A = TRANS_WP[2];
      }
      else
      {
        if (cur_x < (HAGV[0] - 0.2))
        {
          goal_x = cur_x;
          goal_y = cur_y;
          goal_A = cur_A;
        }
        else if (cur_x > (MID_WP[0] - 0.5))
        {
          goal_x = MID_WP[0];
          goal_y = MID_WP[1];
          goal_A = target_A;
        }
        else
        {
          goal_x = cur_x;
          goal_y = cur_y;
          goal_A = target_A;
        }
      }
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    count = 1;
    agvStatus_ = LRF_RUN;
    if ((agvGoal_ == CONV) || (agvGoal_ == GND))
    {
      LVX_MAX = 0.25;
      LVY_MAX = 0.15;
    }
    else
    {
      LVX_MAX = 0.35;
      LVY_MAX = 0.25;
    }
  }
  nav_control();
  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    count = 0;
    pt_++;
  }
}

void NavigationManagement::WP_MID2_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    if (current_y >= (RB1_WP[1] - 0.5))
    {
      if (agvGoal_ == RB1)
      {
        goal_x = cur_x;
        goal_y = RB1_WP[1];
        goal_A = target_A;
      }
      else
      {
        goal_x = TRANS_WP[0];
        goal_y = TRANS_WP[1];
        goal_A = TRANS_WP[2];
      }
    }
    else
    {
      if (agvGoal_ == RB1)
      {
        goal_x = TRANS_WP[0];
        goal_y = TRANS_WP[1];
        goal_A = target_A;
      }
      else if (agvGoal_ == RB2 || agvGoal_ == RB3)
      {
        goal_x = MID_WP[0];
        goal_y = cur_y;
        goal_A = target_A;
      }
      else if (agvGoal_ == H_AGV)
      {
        goal_x = HAGV[0];
        goal_y = MID_WP[1];
        goal_A = target_A;
      }
      else if (agvGoal_ == RA1 || agvGoal_ == RA2 || agvGoal_ == RA3 ||
               agvGoal_ == RA4)
      {
        goal_x = target_pos_x;
        goal_y = MID_WP[1];
        goal_A = target_A;
      }
      else if (agvGoal_ == CONV)
      {
        goal_x = target_pos_x;
        goal_y = MID_WP[1];
        goal_A = target_A;
      }
      else if (agvGoal_ == CHG)
      {
        goal_x = target_pos_x;
        goal_y = MID_WP[1];
        goal_A = target_A;
      }
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    count = 1;
    agvStatus_ = LRF_RUN;

    // Add 210825
    if (agvGoal_ == RB2 || agvGoal_ == RB3 || agvGoal_ == RB1 ||
        agvGoal_ == CONV || agvGoal_ == GND)
    {
      mode_conv_ = conv_zhome;
    }
    if ((agvGoal_ == CONV) || (agvGoal_ == GND))
    {
      LVX_MAX = 0.25;
      LVY_MAX = 0.15;
    }
    else
    {
      LVX_MAX = 0.35;
      LVY_MAX = 0.25;
    }
  }
  nav_control();
  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    count = 0;
    pt_++;
  }
}

void NavigationManagement::WP_MID3_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;

    if (agvGoal_ == RB1)
    {
      if (current_y >= (RB1_WP[1] - 0.5))
      {
        goal_x = cur_x;
        goal_y = cur_y;
        goal_A = target_A;
      }
      else
      {
        goal_x = RB1_WP[0];
        goal_y = RB1_WP[1];
        goal_A = RB1_WP[2];
      }
    }
    else
    {
      if (cur_x > (TRANS_WP[0] - 0.3) && cur_y > (TRANS_WP[1] - 0.3))
      {
        if (agvGoal_ == CHG && (cur_A > 80 && cur_A < 100))
        {
          goal_x = cur_x;
          goal_y = cur_y;
          goal_A = current_A;
        }
        else
        {
          goal_x = MID_WP[0];
          goal_y = MID_WP[1];
          goal_A = target_A;
        }
      }
      else
      {
        goal_x = cur_x;
        goal_y = cur_y;
        goal_A = target_A;
      }
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    count = 1;
    agvStatus_ = LRF_RUN;

    if ((agvGoal_ == CONV) || (agvGoal_ == GND))
    {
      LVX_MAX = 0.25;
      LVY_MAX = 0.15;
    }
    else
    {
      LVX_MAX = 0.35;
      LVY_MAX = 0.25;
    }
  }
  nav_control();
  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    count = 0;
    pt_++;
  }
}

void NavigationManagement::wp_goal_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;

    if (agvGoal_ == H_AGV || agvGoal_ == RB1 || agvGoal_ == RA1 ||
        agvGoal_ == RA2 || agvGoal_ == RA3 || agvGoal_ == RA4 ||
        agvGoal_ == CONV || agvGoal_ == CHG)
    {
      goal_x = target_pos_x;
      goal_y = cur_y;
      goal_A = target_A;
    }
    else if (agvGoal_ == RB2 || agvGoal_ == RB3 || agvGoal_ == GND)
    {
      goal_x = cur_x;
      goal_y = target_pos_y;
      goal_A = target_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    count = 1;
    agvStatus_ = LRF_RUN;

    if ((agvGoal_ == CONV) || (agvGoal_ == GND))
    {
      LVX_MAX = 0.25;
      LVY_MAX = 0.15;
    }
    else
    {
      LVX_MAX = 0.35;
      LVY_MAX = 0.25;
    }
  }
  nav_control();
  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    count = 0;
    pt_++;
  }
}

void NavigationManagement::goal_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = target_pos_x;
    goal_y = target_pos_y;
    goal_A = target_A;

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;

    if ((agvGoal_ == CONV) || (agvGoal_ == GND))
    {
      LVX_MAX = 0.25;
      LVY_MAX = 0.15;
    }
    else if(agvGoal_ == CHG)
    {
       LVY_MAX = 0.25;
       LVX_MAX = 0.1;
    }
    else
    {
      LVX_MAX = 0.35;
      LVY_MAX = 0.25;
    }
  }
  nav_control();
  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    count = 0;
    wp_ = NO_POINT_;
    agvPos_ = agvGoal_;
    moveMode_ = MOVE_ZERO_;
    mode_agv_ = mode_stop_;
    agvStatus_ = GOAL_REACH;
    pt_ = 0;
    pts_ = 0;
    if (agvGoal_ == CHG)
    {
      std_msgs::UInt8 chg_data;
      chg_data.data = 1;
      pub_CHG.publish(chg_data);
    }
  }
  /*
  else if(agvStatus_ == WP_REACH)
  {
      moveMode_ = MOVE_QR_;
      lrfMove_ = LRF_NONE_;
  }
  else if (agvStatus_ == QR_COMP)
  {
    count = 0;
    wp_ = NO_POINT_;
    agvPos_ = agvGoal_;
    moveMode_ = MOVE_ZERO_;
    mode_agv_ = mode_stop_;
    agvStatus_ = GOAL_REACH;
    pt_ = 0;
    pts_ = 0;
    if (agvGoal_ == CHG)
    {
      std_msgs::UInt8 chg_data;
      chg_data.data = 1;
      pub_CHG.publish(chg_data);
    }
  }
  */
}

void NavigationManagement::diag_control()
{
  delX_ = goal_x - current_x;
  lenX_ = fabs(goal_x - cur_x);
  // vel_.angular.z = 0;

  if (fabs(delX_) <= delX_TOL)
  {
    vel_.linear.x = 0;
    vel_.linear.y = 0;
    vel_.angular.z = 0;
    agvStatus_ = LRF_XY_COMP;
  }
  else
  {
    agvStatus_ = LRF_RUN;
    if (cur_A > 20 && cur_A < 70) // Around 45 deg
    {
      if (lenX_ > 2)
        vel_.linear.x = lrf_tracking_ctrl(Kp_, delX_, LVX_MAX, 0.005);
      else
        vel_.linear.x = lrf_tracking_ctrl(Kp_, delX_, 0.4, 0.005);
    }
    else if ((cur_A > 200 && cur_A <= 250) || (cur_A < -160 && cur_A >= -120))
    {
      if (lenX_ > 2)
        vel_.linear.x = -lrf_tracking_ctrl(Kp_, delX_, LVX_MAX, 0.005);
      else
        vel_.linear.x = -lrf_tracking_ctrl(Kp_, delX_, 0.4, 0.005);
    }
    else
    {
      vel_.linear.x = 0;
    }
    vel_.angular.z = 0;
    vel_.linear.y = 0;
  }
}

void NavigationManagement::stop_agv()
{
  vel_.linear.x = 0;
  vel_.linear.y = 0;
  vel_.angular.z = 0;
}

void NavigationManagement::param_set()
{

  if (nh.getParam("/HAGV", HAGV))
  {
    for (vector<int>::size_type i = 0; i < HAGV.size(); ++i)
    {
      ROS_INFO("HAGV[%d] : %.2f", i, HAGV[i]);
    }
  }
  if (nh.getParam("/CHARGE", CHARGE))
  {
    for (vector<int>::size_type i = 0; i < CHARGE.size(); ++i)
    {
      ROS_INFO("CHARGE[%d] : %.2f", i, CHARGE[i]);
    }
  }
  if (nh.getParam("/MID_WP", MID_WP))
  {
    for (vector<int>::size_type i = 0; i < MID_WP.size(); ++i)
    {
      ROS_INFO("MID_WP[%d] : %.2f", i, MID_WP[i]);
    }
  }
  if (nh.getParam("/RB1_WP", RB1_WP))
  {
    for (vector<int>::size_type i = 0; i < RB1_WP.size(); ++i)
    {
      ROS_INFO("RB1_WP[%d] : %.2f", i, RB1_WP[i]);
    }
  }
  if (nh.getParam("/TRANS_WP", TRANS_WP))
  {
    for (vector<int>::size_type i = 0; i < TRANS_WP.size(); ++i)
    {
      ROS_INFO("CONV[%d] : %.2f", i, TRANS_WP[i]);
    }
  }
}

} // namespace agv_wp

int main(int argc, char** argv)
{
  ros::init(argc, argv, "AGV_nav_action");

  agv_wp::NavigationManagement AGV_nav;
  AGV_nav.spin();

  return 0;
}
