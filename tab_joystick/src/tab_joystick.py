#!/usr/bin/env python3.8

import rospy
import datetime
import multiprocessing as mp

from tcp_socket import *
from typing import Dict, List, Any
from geometry_msgs.msg import Pose2D
from std_msgs.msg import Bool, Int8
from move_control.msg import teleop, convJoy


class TabJoystick:
    agv_no: int = 1     # AGV 번호
    agv_ip_address: str = "10.10.26.91"
    joy_port_number: int = 7500
    slow_scale: float = 10.0

    joy_emergency_stop: bool = False             # 조이스틱 비상 정지 여부
    joy_emergency_mode: bool = False             # 조이스틱 비상 모드 여부
    joy_precision_mode: bool = False             # 정밀 모드 여부
    manual_mode: bool = False

    current_pose: Pose2D = Pose2D()             # 현재 위치

    x_speed: float = 0.0
    y_speed: float = 0.0
    angle_speed: float = 0.0
    conv_speed: float = 0.0

    now_time: datetime.datetime = datetime.datetime.now()

    cmd_vel_joy_data: teleop = teleop()

    # ====================================================
    #                  ROS Publish 부분
    # ====================================================
    cmd_vel_pub: rospy.Publisher                # 바퀴 이동 Publish           
    conv_pub: rospy.Publisher                   # 컨베이어 이동 Publish 
    joy_connect_pub: rospy.Publisher            # 조이스틱 연결 상태 Publish 
    joy_precise_pub: rospy.Publisher            # 조이스틱 정밀 모드 Publish 
    joy_emergency_stop_pub: rospy.Publisher     # 조이스틱 비상 정지 Publish 
    joy_emergency_mode_pub: rospy.Publisher     # 조이스틱 비상 모드 Publish 

    # ====================================================
    #                  소켓 송/수신 부분
    # ====================================================
    tcp_client_processing: mp.Queue
    receive_time: datetime.datetime = datetime.datetime.now()   # 전문 수신 시간
    send_time: datetime.datetime = datetime.datetime.now()      # 전문 송신 시간

    client_send_queue: mp.Queue = mp.Queue()          # 송신 데이터 큐
    client_received_queue:  mp.Queue = mp.Queue()     # 수신 데이터 큐
    agv_client_connect = False                        # 클라이언트 연결 상태

    def __init__(self):
        rospy.init_node('tab_joystick')

        # Set Variable
        rate = rospy.Rate(10) #10hz

        # Parameter 가져오기
        self.agv_no = int(rospy.get_param("/agv_no", self.agv_no))
        self.agv_ip_address = str(rospy.get_param("/agv_ip_address", self.agv_ip_address))
        self.joy_port_number = int(rospy.get_param("~joy_port_number", self.joy_port_number))
        self.slow_scale = int(rospy.get_param("~slow_scale", self.slow_scale))

        # Set Publish
        self.cmd_vel_pub = rospy.Publisher("/cmd_vel_joy", teleop, queue_size=10)
        self.conv_pub = rospy.Publisher("/CONV/MAN", convJoy, queue_size=10)
        self.joy_connect_pub = rospy.Publisher("/joy/CONNECT", Bool, queue_size=10)
        self.joy_precise_pub = rospy.Publisher("/joy/PRECISE", Bool, queue_size=10)
        self.joy_emergency_stop_pub = rospy.Publisher("/EMERGENCY/JOY", Bool, queue_size=10)
        self.joy_emergency_mode_pub = rospy.Publisher("/EMERGENCY/JOY/MODE", Bool, queue_size=10)

        # Set Subscriber
        rospy.Subscriber("/agv_pose", Pose2D, self.agvPoseCallback)                 # 현재 위치 설정

        # 소켓 서버 시작
        self.tcp_server_processing = mp.Process(target=self.socket_server_start, args=(
            self.agv_ip_address, self.joy_port_number, "tab_joy", self.client_received_queue, self.client_send_queue))
        self.tcp_server_processing.start()

        # Loop
        while not rospy.is_shutdown():
            self.update()
            rate.sleep()

        self.tcp_server_processing.kill()

    # 반복
    def update(self):
        self.now_time = datetime.datetime.now()
        self.socketDataProcess()    # 수신 데이터 처리
        self.timerCheck()           # 시간 체크
        self.joyPub()               # 조이스틱 Publish
            
    # 타이머 체크
    # 2초 마다 Healthy 전문 송신
    # 1초 마다 AGV 상태 송신
    # 10초 지나도록 Healthy 전문 수신 못받을 시 재접속
    def timerCheck(self):
        send_diff: datetime.datetime = self.now_time - self.send_time           # 전문 송신 시간 차이
        receive_diff: datetime.datetime = self.now_time - self.receive_time     # 전문 수신 시간 차이

        if self.agv_client_connect is True and send_diff.seconds >= 1:
            # 전문 보내고 1초 지나면 송신
            self.sendDataProcess()
            self.send_time = self.now_time

        # if self.agv_client_connect is True and receive_diff.seconds >= 5:
        #     # 5초 지나도록 전문 수신 못하면 재접속
        #     self.agv_client_connect = False
        #     self.client_send_queue.put(bytes(0))

        #     log_data: str = f"5초가 지나도록 전문을 받지 못했습니다. TCP 서버 재접속 요청합니다."
        #     rospy.loginfo(log_data)

    # 조이스틱 Publish
    def joyPub(self):
        # 컨베이어 속도
        conv_speed_data: convJoy = convJoy()
        conv_speed_data.mode = 0
        conv_speed_data.speed = 0

        joy_connect_data: Bool = Bool(self.agv_client_connect)      # 조이스틱 연결 여부
        joy_precise_data: Bool = Bool(False)                        # 정밀 모드 여부 
        joy_emergency_stop_data: Bool = Bool(False)                 # 비상 정지 여부 
        joy_emergency_mode_data: Bool = Bool(False)                 # 비상 모드 여부 

        if self.agv_client_connect is True:
            # 서버가 연결되어있을 경우
            joy_precise_data.data = self.joy_precision_mode               # 정밀 모드 여부 
            joy_emergency_stop_data.data = self.joy_emergency_stop      # 비상 정지 여부
            joy_emergency_mode_data.data = self.joy_emergency_mode      # 비상 모드 여부

            # =================================
            #           속도 지정
            # =================================
            if self.manual_mode is True:
                # 메뉴얼 모드가 참일 경우
                self.cmd_vel_joy_data.linear_x = self.x_speed
                self.cmd_vel_joy_data.linear_y = self.y_speed
                self.cmd_vel_joy_data.angular_z = self.angle_speed
                self.cmd_vel_joy_data.control_mode = 1

                conv_speed_data.mode = 1
                conv_speed_data.speed = self.conv_speed
                
                if self.joy_precision_mode is True:
                    # 정밀모드라면
                    self.cmd_vel_joy_data.linear_x /= self.slow_scale
                    self.cmd_vel_joy_data.linear_y /= self.slow_scale
                    self.cmd_vel_joy_data.angular_z /= self.slow_scale

                    conv_speed_data.speed /= self.slow_scale
        else:
            # 서버가 연결되어있지 않다면
            self.cmd_vel_joy_data.linear_x = 0.0
            self.cmd_vel_joy_data.linear_y = 0.0
            self.cmd_vel_joy_data.angular_z = 0.0
            self.cmd_vel_joy_data.control_mode = 0

        self.cmd_vel_pub.publish(self.cmd_vel_joy_data)     # 휠 모터 속도 보내기
        self.conv_pub.publish(conv_speed_data)              # 컨베이어 속도 보내기
        self.joy_connect_pub.publish(joy_connect_data)      # 조이스틱 연결 여부 보내기
        self.joy_precise_pub.publish(joy_precise_data)      # 조이스틱 정밀 모드 여부 보내기
        self.joy_emergency_stop_pub.publish(joy_emergency_stop_data)      # 조이스틱 비상 정지 여부 보내기
        self.joy_emergency_mode_pub.publish(joy_emergency_mode_data)      # 조이스틱 비상 모드 여부 보내기

    # ====================================================
    #                  소켓 송/수신 부분
    # ====================================================
    # L2 소켓 서버 실행
    @staticmethod
    def socket_server_start(host: str, port: int, socket_type: str, received_queue: mp.Queue, send_queue: mp.Queue):
        print("소켓 서버 실행")
        SocketServer(host, port, socket_type, received_queue, send_queue)

    # 소켓 데이터 처리 함수
    # type => 데이터 타입
    #     info => 소켓 메시지
    #     connect => 연결 여부
    #     error => 소켓 에러
    #     receive => 수신 데이터
    # time => 시간
    # socket_type => 소켓 타입 (예시: acs_client)
    # data => 데이터
    def socketDataProcess(self):
        while self.client_received_queue.qsize() > 0:
            # 수신 데이터가 있을 경우
            received_data: Dict = self.client_received_queue.get()
            socket_data: any = received_data['data']
            data_type: str = received_data['type']
            socket_type: str = received_data['socket_type']
        
            try:
                if data_type == 'error':
                    # 타입이 에러 타입이라면
                    rospy.logerr(str(socket_data))
                elif data_type == 'receive':
                    # 타입이 수신 데이터라면
                    self.receivedDataProcess(received_data)
                elif data_type == 'info':
                    # 타입이 정보 알림일 경우
                    rospy.loginfo(str(socket_data))
                elif data_type == 'connect':
                    # 타입이 연결 정보일 경우
                    self.agv_client_connect = socket_data
                    if self.agv_client_connect is True:
                        # 연결 시 송수신 시간 초기화
                        self.receive_time = self.now_time
                        self.send_time = self.now_time
                else:
                    raise Exception("소켓 데이터의 타입을 알 수 없습니다.")
            except Exception as e: 
                # 예외 발생 시
                log_data: str = f"소켓 수신 데이터를 처리 중 예외가 발생했습니다\n예외 메시지: {e}\n소켓 데이터: {str(received_data)}"
                rospy.loginfo(log_data)

    # ====================================================
    #                  소켓 수신 처리 부분
    # ====================================================
    # 수신 데이터 처리 함수
    def receivedDataProcess(self, received_data: Dict):
        # 받은 데이터 리스트로 변환
        list_socket_data: list = getCharData(bytearray(received_data['data'])).split(',')
        
        # 테스트 용
        rospy.loginfo(f"받은 데이터: {str(list_socket_data)}")

        #if int(list_socket_data[1]) == self.agv_no:     # AGV 번호가 일치 하다면
        self.x_speed = float(list_socket_data[3])     # X축
        self.y_speed = float(list_socket_data[2]) * -1     # Y축
        self.angle_speed = float(list_socket_data[4]) * -1     # 회전
        self.conv_speed = float(list_socket_data[5])     # 왼쪽 조이스틱
        float(list_socket_data[6])     # 오른쪽 조이스틱
        self.manual_mode = True if list_socket_data[7] == '1' else False     # 수동 모드 여부
        self.joy_emergency_stop = True if list_socket_data[8] == '1' else False     # 비상 정지 여부
        self.joy_emergency_mode = True if list_socket_data[9] == '1' else False     # 비상 모드 여부
        self.joy_precision_mode = True if list_socket_data[10] == '1' else False    # 정밀 모드 여부
        self.receive_time = self.now_time

    # ====================================================
    #                  소켓 송신 처리 부분
    # ====================================================
    # 송신 데이터 처리 함수
    def sendDataProcess(self):
        send_data: List[Dict] = [
            {"item_type": 'char', "length": 2, "data_item": "s,"},
            {"item_type": 'char', "length": 1, "data_item": self.agv_no},
            {"item_type": 'char', "length": 1, "data_item": ","},
            {"item_type": 'char', "length": 7, "data_item": self.current_pose.x},
            {"item_type": 'char', "length": 1, "data_item": ","},
            {"item_type": 'char', "length": 7, "data_item": self.current_pose.y},
            {"item_type": 'char', "length": 1, "data_item": ","},
            {"item_type": 'char', "length": 5, "data_item": self.current_pose.theta},
            {"item_type": 'char', "length": 3, "data_item": ",e\n"},
        ]
        protocol_data: bytearray = setSocketData(send_data)

        # 테스트 용
        rospy.loginfo(f"보낸 데이터: {protocol_data}")

        self.client_send_queue.put(bytes(protocol_data))

    # ====================================================
    #                  콜백 함수 정의 부분
    # ====================================================
    # 현재 위치 가져오기
    def agvPoseCallback(self, pose_data: Pose2D):
        self.current_pose = pose_data


if __name__=='__main__':
    TabJoystick()