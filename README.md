# P2104 Posco E&C AGV

AGV Program

date: 2021-12-05


# working : convayor_part


[COM_PORT]

* front-left : /dev/ttyUSB001
* front-right : /dev/ttyUSB002
* rear-left : /dev/ttyUSB003
* rear-right : /dev/ttyUSB004
* convayor : /dev/ttyUSB005
* IMU : /dev/ttyUSB006


[IP_INFO]

#### agv#1
* pc port1: 192.168.2.101 
* pc port2: 192.168.101.1 
* acsys : 192.168.2.201 

#### agv#2
* pc port1: 192.168.2.102 
* pc port2: 192.168.101.2 
* acsys : 192.168.2.202 

#### hub
* front-lidar : 192.168.101.11 
* rear-lidar  : 192.168.101.10 
* sick-lidar  : 192.168.101.20 
